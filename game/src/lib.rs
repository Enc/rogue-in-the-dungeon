//! # Rogue in the Dungeon
//!
//! Rogue in the Dungeon is a traditional fantasy roguelike game.
//!
//! The player delves into a dungeon, fighting the creatures and monsters that inhabit it.
//! The goal is simply to descend as deep as possible before dying.
//!
//! This documentation is a technical reference for the internals of the game engine.

#![forbid(unsafe_code)]

mod dice;
mod game;
mod world_view;

// use rand_xoshiro::Xoshiro256StarStar;
// use slotmap::new_key_type;
// use std::collections::{HashMap, HashSet};

pub use game::{
    AIError, AIResult, Action, ActorEvent, Event, Game, GameError, GameResult, ItemEvent, Player,
    TileEvent,
};
pub use world::{
    Actor, ActorError, ActorKind, Damage, DamageKind, Element, PhysicalDamage, Stat, STATS,
};
pub use world::{
    ActorKey, GameRng, GameSeed, ItemKey, Map, Set, WORLD_COLS, WORLD_DIM, WORLD_ROWS,
};
pub use world::{Armor, Item, ItemKind, Potion, Scroll, Wand, Weapon};
pub use world::{Coord, Dir, ItemCoord, WorldError, DIRS};
pub use world::{Feature, FloorKind, Tile, TileKind, WallKind};
pub use world_view::WorldView;

// /// The chosen RNG type.
// pub type GameRng = Xoshiro256StarStar;

// /// The seed for the RNG.
// pub type GameSeed = [u8; 32];

// new_key_type! {
//     /// The key type for `Actor`s stored in a `SlotMap`.
//     pub struct ActorKey;

//     /// The key type for `Item`s stored in a `SlotMap`.
//     pub struct ItemKey;
// }

// /// The chosen map type.
// pub type Map<K, V> = HashMap<K, V>;

// /// The chosen set type.
// pub type Set<T> = HashSet<T>;

// /// The vertical size of the world map.
// pub const WORLD_ROWS: usize = 35;

// /// The horizontal size of the world map.
// pub const WORLD_COLS: usize = 75;

// /// The size of the world.
// pub const WORLD_DIM: [usize; 2] = [WORLD_ROWS, WORLD_COLS];
