//! This module provides the main tools to create a game and play it.

mod action;
mod ai;
mod error;
mod event;

pub use self::action::Action;
use self::ai::MobAI;
pub use self::ai::{AIError, AIResult, Player};
pub use self::error::{GameError, GameResult};
pub use self::event::{ActorEvent, Event, ItemEvent, TileEvent};
use crate::dice::Die;
use crate::world_view::WorldView;
use crate::{Actor, ActorKind, Damage, DamageKind, Element, PhysicalDamage, Stat};
use crate::{ActorKey, GameRng, GameSeed, ItemKey};
use crate::{Coord, Dir, ItemCoord, WorldError, DIRS};
use crate::{Feature, Tile, TileKind};
use crate::{ItemKind, Potion};
use rand::prelude::*;
use serde::{Deserialize, Serialize};
use slotmap::SecondaryMap;
use world::{Alignment, TimeUnit, World};

/// `Game` owns all the game's data and provides an interface to interact with it and make it progress.
#[derive(Serialize, Deserialize)]
pub struct Game {
    world: World,
    ais: SecondaryMap<ActorKey, Box<dyn Player>>,
    rng: GameRng,
}

impl Game {
    /// Generate a new game from the provided seed.
    pub fn generate(seed: GameSeed, human: Box<dyn Player>) -> Game {
        let mut rng = GameRng::from_seed(seed);

        // generate the world
        let mut world = World::generate(1, &mut rng);

        // generate the player's actor
        let player = Actor::generate(ActorKind::Player, Alignment::Player, &mut rng);
        // add the player's actor in the world
        // place the player on the floor's landing tile
        let landing = world.plan().landing;
        let player_key = world
            .insert_actor(player, landing)
            .expect("insert new actor");
        let mut ais = SecondaryMap::new();
        ais.insert(player_key, human);

        Game {
            // player_key,
            world,
            ais,
            rng,
        }
    }

    pub fn save(&self) -> std::io::Result<()> {
        use std::fs::File;
        use std::io::prelude::*;

        let savegame = bincode::serialize(&self).expect("savegame");
        let mut file = File::create("rogue.dungeon")?;
        file.write_all(&savegame)?;
        Ok(())
    }

    pub fn load_game() -> std::io::Result<Game> {
        use std::fs::{remove_file, File};
        use std::io::prelude::*;
        use std::io::BufReader;

        let file = File::open("rogue.dungeon")?;
        let mut buf_reader = BufReader::new(file);
        let mut contents = Vec::new();
        buf_reader.read_to_end(&mut contents)?;
        let game: Game = bincode::deserialize(&contents).expect("deserialize");
        remove_file("rogue.dungeon")?;
        Ok(game)
    }

    pub fn run(&mut self) -> GameResult<()> {
        loop {
            if let Some(actor_key) = self.world.time().active_actor() {
                let action;
                if let Some(ai) = self
                    .ais
                    .entry(actor_key)
                    .map(|e| e.or_insert(Box::new(MobAI::new())))
                {
                    let active_view = WorldView::new(actor_key, &self.world);
                    match ai.decide(&active_view) {
                        Ok(ai_action) => action = ai_action,
                        Err(ai_error) => match ai_error {
                            AIError::Quit => {
                                self.save().expect("Save game");
                                return Ok(());
                            }
                        },
                    }
                } else {
                    // `SecondaryMap::entry` returns `None` if the key was removed from the originating `SlotMap`.
                    // In such case, just skip this `Entity`.
                    // TODO: assign AI to all actors at initialization,
                    // and make a missing one an error.
                    continue;
                }
                let outcome = self.act(actor_key, action);
                if let Err(GameError::AI {
                    ai_error: AIError::Quit,
                    ..
                }) = outcome
                {
                    return Ok(());
                }
                // We need to get the ai twice, because after `Game::act` the ai might have changed.
                // This is effectively enforced by the borrow-checker.
                if let Some(ai) = self.ais.get_mut(actor_key) {
                    let active_view = WorldView::new(actor_key, &self.world);
                    ai.outcome(&active_view, action, outcome);
                } else {
                    continue;
                }
            } else {
                // If there is no active actor,
                // then it's time for the clock to tick.
                self.time_tick()?;
            }
        }
    }

    fn time_tick(&mut self) -> GameResult<()> {
        self.world.time_tick()?;
        self.expand_fire()?;
        for actor_key in self.world.actors().keys().collect::<Vec<ActorKey>>() {
            self.actor_time_tick(actor_key)?;
        }
        self.broadcast(Event::TimeTick)?;
        Ok(())
    }

    fn expand_fire(&mut self) -> GameResult<()> {
        let on_fire: Vec<Coord> = self
            .world
            .tile_map()
            .tiles()
            .indexed_iter()
            .filter_map(|(idx, tile)| {
                if let Some(Feature::Fire { .. }) = tile.feature() {
                    Some(Coord::from(idx))
                } else {
                    None
                }
            })
            .collect();
        for coord in on_fire {
            for &dir in &DIRS {
                if let Some(fuel) = self
                    .world
                    .tile_map()
                    .tile(coord + dir)
                    .ok()
                    .and_then(Tile::feature)
                    .and_then(Feature::flammable)
                {
                    self.world
                        .with_feature(coord + dir, Some(Feature::Fire { fuel }))
                        .expect("set feature");
                }
            }
        }
        Ok(())
    }

    fn actor_time_tick(&mut self, actor_key: ActorKey) -> GameResult<()> {
        let actor = self.world.actor_mut(actor_key)?;

        // Burn
        if actor.burning() > 0 {
            actor.burn();
            // Queue effect
        }

        // Bleed
        if actor.bleeding() > 0 {
            actor.bleed();
            let coord = self.world.actor_coord(actor_key)?;
            let tile = self.world.tile_map().tile(coord)?;
            if tile.feature().is_none() && tile.steppable() {
                self.world.with_feature(coord, Some(Feature::Blood))?;
            }
        }
        Ok(())
    }

    /// Make `actor_key` perform an action.
    fn act(&mut self, actor_key: ActorKey, action: Action) -> GameResult<()> {
        match action {
            Action::DescendStairs => self.descend_stairs(actor_key)?,
            Action::DropItem(item_key) => self.drop_item(actor_key, item_key)?,
            Action::EquipArmor(item_key) => self.equip_armor(actor_key, item_key)?,
            Action::EquipWeapon(item_key) => self.equip_weapon(actor_key, item_key)?,
            Action::Jump(dir) => self.jump(actor_key, dir)?,
            Action::MeleeAttack(dir) => self.melee_attack(actor_key, dir)?,
            Action::OpenDoor(dir) => self.open_door(actor_key, dir)?,
            Action::Pick => self.pick(actor_key)?,
            Action::PrayAltar { dir, stat } => self.pray_altar(actor_key, dir, stat)?,
            Action::Quaff(item_key) => self.quaff(actor_key, item_key)?,
            Action::Step(dir) => self.step(actor_key, dir)?,
            Action::Throw { item_key, dir } => self.throw(actor_key, item_key, dir)?,
            Action::Wait => {}
            Action::Zap { wand_key, dir } => self.zap(actor_key, wand_key, dir)?,
            _ => todo!(),
        }

        let time_cost = self.time_cost(actor_key, action)?;
        self.world.schedule_actor(actor_key, time_cost)?;

        Ok(())
    }

    fn time_cost(&mut self, actor_key: ActorKey, action: Action) -> GameResult<TimeUnit> {
        const QUICK: TimeUnit = 2;
        const NORMAL: TimeUnit = 3;
        const SLOW: TimeUnit = 5;

        match action {
            Action::Wait => Ok(QUICK),
            Action::MeleeAttack(_)
            | Action::Interact(..)
            | Action::Jump(_)
            | Action::OpenDoor(..)
            | Action::Pick
            | Action::DropItem(_)
            | Action::Throw { .. }
            | Action::Quaff(_)
            | Action::Read(_)
            | Action::Zap { .. } => Ok(NORMAL),
            Action::Step(..) => {
                let actor = self.world.actor(actor_key)?;
                let dex = actor.dexterity() as TimeUnit;
                let time = ((NORMAL * 6) / dex)
                    + if self.rng.gen_ratio((NORMAL * 6) % dex, dex) {
                        1
                    } else {
                        0
                    };

                Ok(time)
            }
            // Action::Throw { item_key, dir } => Ok(3),
            Action::DescendStairs
            | Action::EquipArmor(_)
            | Action::EquipWeapon(_)
            | Action::PrayAltar { .. } => Ok(SLOW),
        }
    }

    fn broadcast(&mut self, event: Event) -> GameResult<()> {
        // AIs react to the event.
        for (actor_key, ai) in self.ais.iter_mut() {
            self.world.actor_exist(actor_key).expect("actor exists");
            let view = WorldView::new(actor_key, &self.world);
            if view.witness(event)? {
                ai.react(&view, &event).map_err(|ai_error| GameError::AI {
                    ai_error,
                    actor_key,
                })?;
            }
        }

        Ok(())
    }

    fn descend_stairs(&mut self, actor_key: ActorKey) -> GameResult<()> {
        let coord = self.world.actor_coord(actor_key)?;
        let tile = self.world.tile_map().tile(coord)?;

        if let TileKind::Stairs = tile.kind() {
            self.broadcast(Event::Actor(actor_key, ActorEvent::DescendStairs))?;
            self.new_floor();
            Ok(())
        } else {
            Err(GameError::from(WorldError::NotStairs(coord)))
        }
    }

    fn step(&mut self, actor_key: ActorKey, dir: Dir) -> GameResult<()> {
        let coord = self.world.actor_coord(actor_key).map_err(GameError::from)? + dir;
        let actor = self.world.actor(actor_key)?;
        let tile = self.world.tile_map().tile(coord)?;
        if tile.impassable() {
            Err(GameError::from(WorldError::Impassable(coord)))
        } else if !tile.steppable() && !actor.kind().flying() {
            Err(GameError::from(WorldError::NotSteppable(coord)))
        } else if let Some(_other_key) = self.world.actor_at(coord) {
            Err(GameError::from(WorldError::NotEmpty(coord)))
        } else {
            // Trample vegetation
            if !actor.kind().flying() {
                if let Some(Feature::Vegetation { .. }) = tile.feature() {
                    self.world
                        .with_feature(coord, Some(Feature::Vegetation { trampled: true }))?;
                }
            }

            self.world
                .step_actor(actor_key, dir)
                .map(|_| ())
                .map_err(GameError::from)?;
            self.broadcast(Event::Actor(actor_key, ActorEvent::Step))?;

            Ok(())
        }
    }

    fn throw(&mut self, actor_key: ActorKey, item_key: ItemKey, dir: Dir) -> GameResult<()> {
        let coord = self.world.actor_coord(actor_key)?;
        let actor = self.world.actor_mut(actor_key)?;
        let item = actor
            .drop_item(item_key)
            .map_err(|actor_error| GameError::Actor {
                actor_key,
                actor_error,
            })?;
        let strength = actor.strength();
        let item_key = self.world.add_item(item, ItemCoord::Fly(coord))?;
        self.broadcast(Event::Throw {
            actor_key,
            item_key,
            dir,
        })?;
        self.fly(item_key, strength, dir)
    }

    fn fly(&mut self, item_key: ItemKey, strength: u8, dir: Dir) -> GameResult<()> {
        let coord = self.world.flying_item_coord(item_key)?;
        if strength == 0 || self.world.tile_map().is_impassable(coord + dir)? {
            self.fall_item(item_key)?;
        } else {
            self.world
                .move_item(item_key, ItemCoord::Fly(coord + dir))?;
            self.broadcast(Event::Item(item_key, ItemEvent::Fly))?;
            if let Some(actor_key) = self.world.actor_at(coord + dir) {
                self.broadcast(Event::Hit {
                    item_key,
                    actor_key,
                })?;
                self.fall_item(item_key)?;
            } else {
                self.fly(item_key, strength - 1, dir)?;
            }
        }
        Ok(())
    }

    fn fall_item(&mut self, item_key: ItemKey) -> GameResult<()> {
        let coord = self.world.flying_item_coord(item_key)?;
        let item = self.world.item(item_key)?;
        if let ItemKind::Potion(_) = item.kind() {
            if self.world.tile_map().is_steppable(coord)? {
                self.broadcast(Event::Item(item_key, ItemEvent::PotionShatter))?;
                let _ = self.world.delete_item(item_key)?;
                let tile = self.world.tile_map().tile(coord)?;
                if tile.feature().is_none() {
                    self.world.with_feature(coord, Some(Feature::Flooded))?;
                }
                return Ok(());
            }
        }
        let item_coord = self.roll_item(coord).expect("roll item to free tile");
        let tile = self.world.tile_map().tile(item_coord)?;
        if tile.steppable() {
            let _ = self
                .world
                .move_item(item_key, ItemCoord::Lay(item_coord))
                .expect("place item on ground");
            Ok(())
        } else if let TileKind::Chasm = tile.kind() {
            self.broadcast(Event::Item(item_key, ItemEvent::Disappear))?;
            let _ = self.world.delete_item(item_key).expect("delete item");
            Ok(())
        } else {
            unreachable!();
        }
    }

    fn equip_armor(&mut self, actor_key: ActorKey, item_key: ItemKey) -> GameResult<()> {
        let actor = self.world.actor_mut(actor_key)?;
        actor
            .equip_armor(item_key)
            .map_err(|actor_error| GameError::Actor {
                actor_key,
                actor_error,
            })?;
        self.broadcast(Event::Actor(actor_key, ActorEvent::Wear(item_key)))
    }

    // fn unequip_armor(&mut self, actor_key: ActorKey) -> GameResult<ItemKey> {
    //     let item_key = self.world.inventory_unequip_armor(actor_key)?;
    //     // self.broadcast(Event::UnequipArmor {
    //     //     actor_key,
    //     //     item_key,
    //     // })?;
    //     Ok(item_key)
    // }

    fn equip_weapon(&mut self, actor_key: ActorKey, item_key: ItemKey) -> GameResult<()> {
        let actor = self.world.actor_mut(actor_key)?;
        actor
            .equip_weapon(item_key)
            .map_err(|actor_error| GameError::Actor {
                actor_key,
                actor_error,
            })?;
        self.broadcast(Event::Actor(actor_key, ActorEvent::Wield(item_key)))
    }

    // fn unequip_weapon(&mut self, actor_key: ActorKey) -> GameResult<ItemKey> {
    //     let item_key = self.world.inventory_unequip_weapon(actor_key)?;
    //     // self.broadcast(Event::UnequipWeapon {
    //     //     actor_key,
    //     //     item_key,
    //     // })?;
    //     Ok(item_key)
    // }

    fn jump(&mut self, actor_key: ActorKey, dir: Dir) -> GameResult<()> {
        let coord = self.world.actor_coord(actor_key)?;
        let tile = self.world.tile_map().tile(coord + dir)?;

        if let TileKind::Chasm = tile.kind() {
            self.broadcast(Event::Actor(actor_key, ActorEvent::Jump))?;
            self.broadcast(Event::Actor(actor_key, ActorEvent::Fall))?;
            self.new_floor();
            let actor = self.world.actor(actor_key)?;
            let amount = Die(actor.max_health_points() - 1).roll(1, &mut self.rng);
            let damage = Damage {
                amount,
                kind: DamageKind::Physical(PhysicalDamage::Bludgeoning),
            };
            self.take_damage(actor_key, damage)?;
            Ok(())
        } else {
            Err(GameError::NoChasm(coord))
        }
    }

    fn melee_attack(&mut self, actor_key: ActorKey, dir: Dir) -> GameResult<()> {
        let target_coord = self.world.actor_coord(actor_key)? + dir;
        let target_key = self
            .world
            .actor_at(target_coord)
            .ok_or(GameError::NoMeleeTarget(target_coord))?;
        self.broadcast(Event::MeleeAttack {
            actor_key,
            target_key,
        })?;
        let actor = self.world.actor(actor_key)?;
        let target = self.world.actor(target_key)?;
        if actor.hit_roll(&mut self.rng) >= target.dodge_roll(&mut self.rng) {
            self.melee_hit(actor_key, target_key)?;
        } else {
            self.broadcast(Event::MeleeMiss {
                actor_key,
                target_key,
            })?;
        }
        Ok(())
    }

    fn melee_hit(&mut self, actor_key: ActorKey, target_key: ActorKey) -> GameResult<()> {
        self.broadcast(Event::MeleeHit {
            actor_key,
            target_key,
        })?;

        let actor = self.world.actor(actor_key)?;
        let mut damage = actor.damage_roll(&mut self.rng);
        let target = self.world.actor(target_key)?;
        let defense = target.armor_roll(damage.kind, &mut self.rng);
        damage.amount = damage.amount.saturating_sub(defense);
        self.take_damage(target_key, damage)?;
        Ok(())
    }

    fn new_floor(&mut self) {
        // self.broadcast(Event::NewLevel(actor_key));
        let ais = &mut self.ais;
        let world = &self.world;
        ais.retain(|actor_key, _| {
            matches!(
                world.actor(actor_key).expect("actor").alignment(),
                Alignment::Player
            )
        });
        self.world.generate_new_floor(&mut self.rng);
    }

    fn open_door(&mut self, actor_key: ActorKey, dir: Dir) -> GameResult<()> {
        let door_coord = self.world.actor_coord(actor_key)? + dir;
        if let Feature::Door { open } = self
            .world
            .tile_map()
            .tile(door_coord)?
            .feature()
            .ok_or(WorldError::NoFeature(door_coord))?
        {
            let door = Feature::Door { open: !open };
            self.world.with_feature(door_coord, Some(door))?;
            Ok(())
        } else {
            Err(GameError::from(WorldError::Uninteracting(door_coord)))
        }
    }

    fn pick(&mut self, actor_key: ActorKey) -> GameResult<()> {
        let actor_coord = self.world.actor_coord(actor_key)?;
        let item_key = self
            .world
            .laying_item_at(actor_coord)
            .ok_or(GameError::NoItem(actor_coord))?;
        let (item, _) = self.world.delete_item(item_key)?;
        let actor = self.world.actor_mut(actor_key)?;
        let item_key = actor.take_item(item).expect("TODO: Handle failure here!");
        self.broadcast(Event::Pick {
            actor_key,
            item_key,
        })
    }

    fn pray_altar(&mut self, actor_key: ActorKey, dir: Dir, stat: Stat) -> GameResult<()> {
        let coord = self.world.actor_coord(actor_key)? + dir;
        let tile = self.world.tile_map().tile(coord)?;

        if let Some(Feature::Altar { used }) = tile.feature() {
            if used {
                Err(GameError::from(WorldError::UsedAltar(coord)))
            } else {
                self.world
                    .with_feature(coord, Some(Feature::Altar { used: true }))?;
                self.broadcast(Event::Actor(actor_key, ActorEvent::Blessed(stat)))?;
                let actor = self.world.actor_mut(actor_key)?;
                actor.increase_stat(stat);
                self.broadcast(Event::Actor(actor_key, ActorEvent::Upgrade(stat)))?;
                Ok(())
            }
        } else {
            Err(GameError::from(WorldError::NoAltar(coord)))
        }
    }

    fn quaff(&mut self, actor_key: ActorKey, item_key: ItemKey) -> GameResult<()> {
        let actor = self.world.actor_mut(actor_key)?;
        let item = actor
            .item(item_key)
            .map_err(|actor_error| GameError::Actor {
                actor_key,
                actor_error,
            })?;
        if let ItemKind::Potion(potion) = item.kind() {
            let _ = actor
                .drop_item(item_key)
                .map_err(|actor_error| GameError::Actor {
                    actor_key,
                    actor_error,
                })?;
            self.broadcast(Event::Actor(actor_key, ActorEvent::Quaff(potion)))?;
            let actor = self.world.actor_mut(actor_key)?;
            match potion {
                Potion::Health => {
                    actor.heal();
                    self.broadcast(Event::Actor(actor_key, ActorEvent::Heal))?;
                }
            }
            Ok(())
        } else {
            Err(GameError::CannotQuaff(item_key))
        }
    }

    fn take_damage(&mut self, actor_key: ActorKey, damage: Damage) -> GameResult<()> {
        self.world.actor_mut(actor_key)?.take_damage(damage);
        self.broadcast(Event::Actor(actor_key, ActorEvent::TakeDamage(damage)))?;
        let actor = self.world.actor(actor_key)?;
        if actor.health_points() == 0 {
            self.die(actor_key)?;
        }
        Ok(())
    }

    fn die(&mut self, actor_key: ActorKey) -> GameResult<()> {
        self.broadcast(Event::Actor(actor_key, ActorEvent::Die))?;
        let actor = self.world.actor(actor_key)?;
        let items = actor.items().keys().collect::<Vec<ItemKey>>();
        for &item_key in &items {
            self.drop_item(actor_key, item_key)?;
        }
        let _ = self.ais.remove(actor_key).expect("remove ai");
        let _ = self.world.remove_actor(actor_key)?;
        Ok(())
    }

    fn drop_item(&mut self, actor_key: ActorKey, item_key: ItemKey) -> GameResult<()> {
        self.broadcast(Event::DropItem {
            actor_key,
            item_key,
        })?;
        let actor = self.world.actor_mut(actor_key)?;
        let item = actor
            .drop_item(item_key)
            .map_err(|actor_error| GameError::Actor {
                actor_key,
                actor_error,
            })?;
        let item_coord = self.roll_item(self.world.actor_coord(actor_key)?)?;
        let tile = self.world.tile_map().tile(item_coord)?;
        if tile.steppable() {
            let _ = self.world.add_item(item, ItemCoord::Lay(item_coord));
            Ok(())
        } else if let TileKind::Chasm = tile.kind() {
            self.broadcast(Event::Item(item_key, ItemEvent::Disappear))?;
            let _ = self.world.delete_item(item_key)?;
            Ok(())
        } else {
            unreachable!();
        }
    }

    fn roll_item(&self, coord: Coord) -> GameResult<Coord> {
        let weight_map =
            self.world.tile_map().impassable_map().map(
                |impassable| {
                    if *impassable {
                        None
                    } else {
                        Some(1)
                    }
                },
            );
        let can_place_item = |coord: Coord| {
            !self
                .world
                .tile_map()
                .is_impassable(coord)
                .expect("steppable")
                && self.world.laying_item_at(coord).is_none()
        };
        let dijkstra_map = coord.dijkstra_map(&weight_map, None);
        Ok(dijkstra_map
            .indexed_iter()
            .filter(|(idx, dist)| can_place_item(Coord::from(*idx)) && dist.is_some())
            .min_by_key(|(_, dist)| dist.expect("distance"))
            .map(|(idx, _)| Coord::from(idx))
            .expect("coord where item falls"))
    }

    fn zap(&mut self, actor_key: ActorKey, wand_key: ItemKey, dir: Dir) -> GameResult<()> {
        use world::Wand;

        let actor = self.world.actor_mut(actor_key)?;
        let item = actor
            .item(wand_key)
            .map_err(|actor_error| GameError::Actor {
                actor_key,
                actor_error,
            })?;

        if let ItemKind::Wand(wand) = item.kind() {
            // let actor = self.world.actor(actor_key)?;
            match wand {
                Wand::Lightning => self.cast_lightning(actor_key, dir)?,
                Wand::FireBolt => self.cast_firebolt(actor_key, dir)?,
            }
            self.broadcast(Event::Zap {
                actor_key,
                wand_key,
                dir,
            })?;
            // TODO: wand charges
            Ok(())
        } else {
            Err(GameError::from(WorldError::UnusableItem(wand_key)))
        }
    }

    fn cast_firebolt(&mut self, actor_key: ActorKey, dir: Dir) -> GameResult<()> {
        self.broadcast(Event::Actor(actor_key, ActorEvent::CastFireBolt))?;
        let intensity = self.world.actor(actor_key)?.intelligence();
        let coord = self.world.actor_coord(actor_key)?;
        self.firebolt(coord + dir, dir, intensity)?;
        Ok(())
    }

    fn cast_lightning(&mut self, actor_key: ActorKey, dir: Dir) -> GameResult<()> {
        self.broadcast(Event::Actor(actor_key, ActorEvent::CastLightning))?;
        let intensity = self.world.actor(actor_key)?.intelligence();
        let coord = self.world.actor_coord(actor_key)?;
        self.lightning(coord + dir, dir, intensity)?;
        Ok(())
    }

    fn firebolt(&mut self, coord: Coord, dir: Dir, intensity: u8) -> GameResult<()> {
        // consequences.push(Command::Burn(coord));
        self.broadcast(Event::Tile(coord, TileEvent::FireBolt))?;
        if self.world.actor_at(coord).is_some()
            || self.world.tile_map().is_impassable(coord + dir)?
        {
            // consequences.push(Command::FireBlast { coord, intensity });
            self.fireblast(coord, intensity)?;
        } else {
            self.burn(coord, intensity)?;
            self.firebolt(coord + dir, dir, intensity)?;
        }
        Ok(())
    }

    fn fireblast(&mut self, coord: Coord, intensity: u8) -> GameResult<()> {
        let fire_map = coord.l1_fov(&self.world.tile_map().impassable_map(), intensity / 3);
        for (idx, _) in fire_map.indexed_iter().filter(|(_, &fire)| fire) {
            let coord = Coord::from(idx);
            self.burn(coord, intensity)?;
        }
        Ok(())
    }

    fn burn(&mut self, coord: Coord, intensity: u8) -> GameResult<()> {
        self.burn_tile(coord)?;
        if let Some(actor_key) = self.world.actor_at(coord) {
            self.burn_actor(actor_key, intensity)?;
        }
        if let Some(item_key) = self.world.laying_item_at(coord) {
            self.burn_item(item_key)?;
        }
        Ok(())
    }

    fn burn_actor(&mut self, actor_key: ActorKey, intensity: u8) -> GameResult<()> {
        self.broadcast(Event::Actor(actor_key, ActorEvent::Burn))?;
        let damage = Damage {
            amount: intensity,
            kind: DamageKind::Elemental(Element::Fire),
        };
        self.take_damage(actor_key, damage)?;
        Ok(())
    }

    fn burn_item(&mut self, item_key: ItemKey) -> GameResult<()> {
        if let ItemKind::Scroll(_) = self.world.item(item_key)?.kind() {
            self.world.delete_item(item_key)?;
            self.broadcast(Event::Item(item_key, ItemEvent::Burn))?;
        }
        Ok(())
    }

    fn burn_tile(&mut self, coord: Coord) -> GameResult<()> {
        if let Some(fuel) = self
            .world
            .tile_map()
            .tile(coord)?
            .feature()
            .and_then(Feature::flammable)
        {
            self.world
                .with_feature(coord, Some(Feature::Fire { fuel }))?;
        }
        self.broadcast(Event::Tile(coord, TileEvent::Burn))?;
        Ok(())
    }

    fn lightning(&mut self, coord: Coord, dir: Dir, intensity: u8) -> GameResult<()> {
        self.broadcast(Event::Tile(coord, TileEvent::Lightning))?;
        if let Some(actor_key) = self.world.actor_at(coord) {
            self.fulminate(actor_key, intensity)?;
        }
        if !self.world.tile_map().tile(coord)?.impassable() {
            self.lightning(coord + dir, dir, intensity)?;
        }
        Ok(())
    }

    fn fulminate(&mut self, actor_key: ActorKey, intensity: u8) -> GameResult<()> {
        const ELECTRICITY_DRY_MULT: usize = 3;
        const ELECTRICITY_WET_MULT: usize = 5;

        self.broadcast(Event::Actor(actor_key, ActorEvent::Fulminate))?;

        let coord = self.world.actor_coord(actor_key)?;
        let num;
        if let Some(Feature::Flooded) = self.world.tile_map().tile(coord)?.feature() {
            num = ELECTRICITY_WET_MULT;
        } else {
            num = ELECTRICITY_DRY_MULT;
        }
        let damage = Damage {
            amount: Die(intensity).roll(num, &mut self.rng),
            kind: DamageKind::Elemental(Element::Electricity),
        };
        self.take_damage(actor_key, damage)?;
        Ok(())
    }
}
