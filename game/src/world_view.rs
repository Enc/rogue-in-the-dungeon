use crate::Actor;
use crate::Item;
use crate::{Tile, TileKind};
// use crate::world::{World, WorldError, WorldResult};
use super::{ActorEvent, Event};
use crate::{ActorKey, ItemKey};
use ndarray::{Array2, Ix2};
use world::{Coord, ItemCoord, World, WorldError, WorldResult};

pub struct WorldView<'w> {
    actor_pov: ActorKey,
    world: &'w World,
}

impl<'w> WorldView<'w> {
    pub fn new(actor_pov: ActorKey, world: &'w World) -> WorldView<'w> {
        WorldView { actor_pov, world }
    }

    pub fn pov_key(&self) -> ActorKey {
        self.actor_pov
    }

    pub fn pov_actor(&self) -> &Actor {
        self.world.actor(self.actor_pov).expect("own pov actor")
    }

    pub fn sight_map(&self) -> &Array2<bool> {
        self.world.sight_map(self.actor_pov).expect("sight map")
    }

    pub fn can_see_tile(&self, coord: Coord) -> WorldResult<bool> {
        // if self is alive {
        self.world.can_see_tile(self.actor_pov, coord)
        // } else {
        // Ok(true)
        // }
    }

    pub fn can_remember_tile(&self, coord: Coord) -> WorldResult<bool> {
        // if let Some(actor_key) = self.actor_pov {
        self.world
            .memory_map(self.actor_pov)
            .ok_or(WorldError::NoMemoryMap(self.actor_pov))?
            .get::<Ix2>(coord.into())
            .cloned()
            .ok_or(WorldError::MissingTile(coord))
        // } else {
        // Ok(true)
        // }
    }

    pub fn see_tile(&self, coord: Coord) -> WorldResult<Option<&Tile>> {
        // if self.see_tile(coord)? {
        if self.can_see_tile(coord)? {
            // || self.see_tile(coord)? // a tile seen is always remembered
            self.world.tile_map().tile(coord).map(Some)
        } else {
            Ok(None)
        }
    }

    pub fn remember_tile(&self, coord: Coord) -> WorldResult<Option<TileKind>> {
        if self.can_remember_tile(coord)? {
            self.world.tile_map().tile(coord).map(Tile::kind).map(Some)
        } else {
            Ok(None)
        }
    }

    pub fn see_actor(&self, actor_key: ActorKey) -> WorldResult<bool> {
        if let Ok(coord) = self.world.actor_coord(actor_key) {
            let can_see = self.can_see_tile(coord)?; // && self.is_lit(coord)?;
            Ok(can_see)
        } else {
            Ok(false)
        }
    }

    pub fn actor(&self, actor_key: ActorKey) -> WorldResult<Option<&Actor>> {
        if self.see_actor(actor_key)? {
            self.world.actor(actor_key).map(Some)
        } else {
            Ok(None)
        }
    }

    pub fn actor_at(&self, floor_coord: Coord) -> WorldResult<Option<ActorKey>> {
        if let Some(actor_key) = self.world.actor_at(floor_coord) {
            if self.see_actor(actor_key)? {
                Ok(Some(actor_key))
            } else {
                Ok(None)
            }
        } else {
            Ok(None)
        }
    }

    pub fn actor_coord(&self, actor_key: ActorKey) -> WorldResult<Coord> {
        self.see_actor(actor_key)?;
        self.world.actor_coord(actor_key)
    }

    pub fn steppable_map(&self) -> &Array2<bool> {
        // if let Some(actor_key) = self.actor_pov {
        if self.world.memory_map(self.actor_pov).is_none() {
            // if let Ok(coord) = self.world.map().actor_coord(actor_key) {
            self.world.steppable_map()
        // }
        } else {
            unimplemented!()
        }
        // } else {
        //     self.world.steppable_map()
        // }
    }

    pub fn impassable_map(&self) -> &Array2<bool> {
        // if let Some(actor_key) = self.actor_pov {
        if self.world.memory_map(self.actor_pov).is_none() {
            // if let Ok(coord) = self.world.map().actor_coord(actor_key) {
            self.world.impassable_map()
        // }
        } else {
            unimplemented!()
        }
        // } else {
        //     self.world.steppable_map()
        // }
    }

    pub fn see_item(&self, item_key: ItemKey) -> WorldResult<bool> {
        match self.world.item_location(item_key)? {
            ItemCoord::Lay(coord) => self.can_see_tile(coord),
            ItemCoord::Fly(coord) => self.can_see_tile(coord),
        }
    }

    pub fn item(&self, item_key: ItemKey) -> WorldResult<Option<&Item>> {
        if self.see_item(item_key)? {
            self.world.item(item_key).map(Some)
        } else {
            Ok(None)
        }
    }

    pub fn item_at(&self, floor_coord: Coord) -> WorldResult<Option<ItemKey>> {
        let opt_item = self
            .world
            .laying_item_at(floor_coord)
            .filter(|item_key| self.see_item(*item_key).expect("see item"));
        Ok(opt_item)
    }

    pub fn flying_item_at(&self, floor_coord: Coord) -> WorldResult<Option<ItemKey>> {
        let opt_item = self
            .world
            .flying_item_at(floor_coord)
            .filter(|item_key| self.see_item(*item_key).expect("see item"));
        Ok(opt_item)
    }

    pub fn witness(&self, event: Event) -> WorldResult<bool> {
        match event {
            Event::Actor(actor_key, _) => self.see_actor(actor_key),
            Event::Tile(coord, _) => self.can_see_tile(coord),
            Event::MeleeAttack {
                actor_key,
                target_key,
            }
            | Event::MeleeHit {
                actor_key,
                target_key,
            }
            | Event::MeleeMiss {
                actor_key,
                target_key,
            } => Ok(self.see_actor(actor_key)? && self.see_actor(target_key)?),
            _ => Ok(true),
        }
    }
}
