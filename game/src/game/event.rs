use crate::Potion;
use crate::{ActorKey, ItemKey};
use crate::{Coord, Dir};
use crate::{Damage, Stat};
// use world::TimeUnit;

#[non_exhaustive]
#[derive(Clone, Copy, Debug)]
pub enum ActorEvent {
    Bleed,
    Blessed(Stat),
    Burn,
    CastFireBolt,
    CastLightning,
    DescendStairs,
    Die,
    Fall,
    Fulminate,
    Heal,
    Jump,
    Quaff(Potion),
    // Scream,
    Step,
    TakeDamage(Damage),
    Upgrade(Stat),
    // Wait(TimeUnit)
    Wear(ItemKey),
    Wield(ItemKey),
}

#[non_exhaustive]
#[derive(Clone, Copy, Debug)]
pub enum ItemEvent {
    Burn,
    Disappear,
    Fly,
    PotionShatter,
}

#[non_exhaustive]
#[derive(Clone, Copy, Debug)]
pub enum TileEvent {
    Burn,
    FireBolt,
    Lightning,
}

#[non_exhaustive]
#[derive(Clone, Copy, Debug)]
pub enum Event {
    Actor(ActorKey, ActorEvent),
    Item(ItemKey, ItemEvent),
    Tile(Coord, TileEvent),
    MeleeAttack {
        actor_key: ActorKey,
        target_key: ActorKey,
    },
    MeleeHit {
        actor_key: ActorKey,
        target_key: ActorKey,
    },
    MeleeMiss {
        actor_key: ActorKey,
        target_key: ActorKey,
    },
    DropItem {
        actor_key: ActorKey,
        item_key: ItemKey,
    },
    Hit {
        actor_key: ActorKey,
        item_key: ItemKey,
    },
    Pick {
        actor_key: ActorKey,
        item_key: ItemKey,
    },
    // Read {
    //     actor_key: ActorKey,
    //     scroll_key: ItemKey,
    // },
    Throw {
        actor_key: ActorKey,
        item_key: ItemKey,
        dir: Dir,
    },
    TimeTick,
    Zap {
        actor_key: ActorKey,
        wand_key: ItemKey,
        dir: Dir,
    },
}
