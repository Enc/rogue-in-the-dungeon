use super::{Action, ActorEvent, Event, GameResult, GameRng};
use crate::world_view::WorldView;
use crate::{Coord, Dir, GameSeed, DIRS, WORLD_DIM};
use ndarray::{Array2, Ix2};
use rand::prelude::*;
use serde::{Deserialize, Serialize};
use world::{Actor, Alignment};

#[derive(Debug)]
pub enum AIError {
    Quit,
}

// impl Error for AIError {}

pub type AIResult<T> = Result<T, AIError>;

#[typetag::serde(tag = "type")]
pub trait Player {
    fn decide(&mut self, world_view: &WorldView) -> AIResult<Action>;

    fn react(&mut self, world_view: &WorldView, event: &Event) -> AIResult<()>;

    fn outcome(&mut self, world_view: &WorldView, action: Action, outcome: GameResult<()>);
}

#[derive(Debug, Serialize, Deserialize)]
pub struct MobAI {
    target: Option<Coord>,
    rng: GameRng,
}

impl MobAI {
    const ACTOR_WEIGTH: u8 = 15;
    const MAX_DIST: Option<u8> = Some(30);

    pub fn new() -> MobAI {
        let seed: GameSeed = random();
        MobAI {
            target: None,
            rng: GameRng::from_seed(seed),
        }
    }

    fn update_target(&mut self, view: &WorldView) -> AIResult<()> {
        let self_coord = view.actor_coord(view.pov_key()).expect("self position");
        self.target = view
            .sight_map()
            .indexed_iter()
            .filter_map(|(idx, see)| {
                let coord = Coord::from(idx);
                if *see {
                    if let Some(actor_key) = view.actor_at(coord).expect("actor at coord") {
                        if let Some(Alignment::Player) = view
                            .actor(actor_key)
                            .expect("actor at coord")
                            .map(Actor::alignment)
                        {
                            return Some(coord);
                        }
                    }
                }
                None
            })
            .min_by_key(|&coord| Coord::distance(coord, self_coord))
            .or(self.target);
        Ok(())
    }
}

#[typetag::serde]
impl Player for MobAI {
    fn decide(&mut self, view: &WorldView) -> AIResult<Action> {
        let actor = view.pov_actor();
        let self_coord = view.actor_coord(view.pov_key()).expect("self position");

        if let Some(target_coord) = self.target {
            if Coord::distance(self_coord, target_coord) == 1 {
                if let Some(target_key) = view.actor_at(target_coord).expect("actor at coord") {
                    if let Some(Alignment::Player) = view
                        .actor(target_key)
                        .expect("actor at coord")
                        .map(Actor::alignment)
                    {
                        return Ok(Action::MeleeAttack(
                            (target_coord - self_coord)
                                .directions_from_center()
                                .pop()
                                .expect("direction to target"),
                        ));
                    }
                }
            }

            let steppable_map = if actor.kind().flying() {
                view.impassable_map().map(|impassable| !impassable)
            } else {
                view.steppable_map().clone()
            };
            let mut weight_map = Array2::from_elem(WORLD_DIM, None);
            for (idx, weigth) in weight_map.indexed_iter_mut() {
                if view
                    .actor_at(Coord::from(idx))
                    .expect("see actor at coord")
                    .is_some()
                {
                    *weigth = Some(Self::ACTOR_WEIGTH);
                } else if steppable_map[idx] {
                    *weigth = Some(1);
                }
            }
            let dijkstra_map = target_coord.dijkstra_map(&weight_map, Self::MAX_DIST);
            let mut dirs: Vec<(Dir, u8)> = DIRS
                .iter()
                .filter_map(|dir| {
                    dijkstra_map
                        .get(Ix2::from(self_coord + *dir))
                        .expect("dijkstra value")
                        .map(|dist| (*dir, dist))
                })
                .collect();
            dirs.shuffle(&mut self.rng);
            if let Some((dir, _)) = dirs.iter().min_by_key(|(_, dist)| *dist) {
                return Ok(Action::Step(*dir));
            }
        }
        Ok(Action::Wait)
    }

    fn react(&mut self, view: &WorldView, event: &Event) -> AIResult<()> {
        match event {
            Event::Actor(actor_key, ActorEvent::Step) if *actor_key == view.pov_key() => {
                let self_coord = view.actor_coord(view.pov_key()).expect("self position");
                self.update_target(view)?;
                if Some(self_coord) == self.target {
                    self.target = None;
                }
                Ok(())
            }
            Event::Actor(actor_key, ActorEvent::Step) => {
                if let Some(Alignment::Player) = view
                    .actor(*actor_key)
                    .expect("view actor")
                    .map(Actor::alignment)
                {
                    self.update_target(view)?;
                }
                Ok(())
            }
            _ => Ok(()),
        }
    }

    fn outcome(&mut self, _world_view: &WorldView, _action: Action, outcome: GameResult<()>) {
        // Do something?
        if outcome.is_err() {
            panic!("AIs chosen action failed");
        }
    }
}
