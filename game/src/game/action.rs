use crate::ItemKey;
use crate::Stat;
use crate::{Coord, Dir};

use serde::{Deserialize, Serialize};

#[non_exhaustive]
#[derive(Clone, Copy, Debug, Serialize, Deserialize)]
pub enum Action {
    DescendStairs,
    DropItem(ItemKey),
    EquipArmor(ItemKey),
    EquipWeapon(ItemKey),
    Interact(Coord),
    Jump(Dir),
    MeleeAttack(Dir),
    OpenDoor(Dir),
    Pick,
    Quaff(ItemKey),
    Read(ItemKey),
    Step(Dir),
    Throw { item_key: ItemKey, dir: Dir },
    PrayAltar { stat: Stat, dir: Dir },
    Wait,
    Zap { wand_key: ItemKey, dir: Dir },
}
