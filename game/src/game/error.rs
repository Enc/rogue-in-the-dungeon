use super::AIError;
use crate::{ActorKey, ItemKey};
use crate::{Coord, WorldError};
use std::error::Error;
use std::fmt;
use world::ActorError;
use world::TimeError;

#[derive(Debug)]
pub enum GameError {
    AI {
        actor_key: ActorKey,
        ai_error: AIError,
    },
    Actor {
        actor_key: ActorKey,
        actor_error: ActorError,
    },
    CannotQuaff(ItemKey),
    CannotRead(ItemKey),
    CannotZap(ItemKey),
    NoActiveActor,
    Running,
    PlayerTurn,
    World(WorldError),
    GameOver,
    InventoryFull(ActorKey),
    NoChasm(Coord),
    NoItem(Coord),
    NoMeleeTarget(Coord),
}

impl fmt::Display for GameError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let error_msg = match self {
            _ => "a game error",
        };
        write!(f, "{}", error_msg)
    }
}

impl Error for GameError {}

impl From<WorldError> for GameError {
    fn from(world_error: WorldError) -> Self {
        GameError::World(world_error)
    }
}

impl From<TimeError> for GameError {
    fn from(time_error: TimeError) -> Self {
        GameError::from(WorldError::Time(time_error))
    }
}

pub type GameResult<T> = Result<T, GameError>;
