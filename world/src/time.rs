mod error;

pub use error::{TimeError, TimeResult};

use crate::{ActorKey, Map};
use serde::{Deserialize, Serialize};
use slotmap::SecondaryMap;
use std::collections::VecDeque;

/// The type of the world's time.
pub type TimeUnit = u32;

/// `World` contains and manages the game world's data.
#[derive(Debug, Serialize, Deserialize)]
pub struct Time {
    actors_time: SecondaryMap<ActorKey, TimeUnit>,
    actors_schedule: Map<TimeUnit, VecDeque<ActorKey>>,
    current_time: TimeUnit,
}

impl Default for Time {
    fn default() -> Self {
        Time {
            actors_time: SecondaryMap::default(),
            actors_schedule: Map::default(),
            current_time: 0,
        }
    }
}

impl Time {
    pub fn current_time(&self) -> TimeUnit {
        self.current_time
    }

    /// Returns an `Actor` scheduled action time.
    pub fn actor_time(&self, actor_key: ActorKey) -> TimeResult<TimeUnit> {
        // Retrieve and return the `Actor`'s scheduled action time.
        // (`Actor`s are supposed to be in the schedule at all times.)
        self.actors_time
            .get(actor_key)
            .cloned()
            .ok_or(TimeError::NotScheduled(actor_key))
    }

    pub fn insert(&mut self, actor_key: ActorKey) -> TimeResult<()> {
        if let Ok(time) = self.actor_time(actor_key) {
            Err(TimeError::AlreadyScheduled { actor_key, time })
        } else {
            let previous_time = self.actors_time.insert(actor_key, self.current_time);
            // .expect_none("the actor has no scheuled time yet"); // UNSTABLE
            assert!(previous_time.is_none());

            self.actors_schedule
                .entry(self.current_time)
                .or_default()
                .push_back(actor_key);
            Ok(())
        }
    }

    pub fn remove(&mut self, actor_key: ActorKey) -> TimeResult<TimeUnit> {
        self.actors_time
            .remove(actor_key)
            .ok_or(TimeError::NotScheduled(actor_key))
    }

    pub fn schedule_actor(
        &mut self,
        actor_key: ActorKey,
        waiting_time: TimeUnit,
    ) -> TimeResult<()> {
        if waiting_time == 0 {
            Err(TimeError::NullSchedule(actor_key))
        } else {
            let time = self.actor_time(actor_key)? + waiting_time;
            let _ = self.actors_time.insert(actor_key, time);
            self.actors_schedule
                .entry(time)
                .or_default()
                .push_back(actor_key);
            self.update_schedule();
            Ok(())
        }
    }

    fn update_schedule(&mut self) {
        while let Some(active_actor) = self.active_actor() {
            if self
                .actors_time
                .get(active_actor)
                .cloned()
                .map(|time| time == self.current_time)
                .unwrap_or(false)
            {
                return;
            } else {
                self.actors_schedule
                    .entry(self.current_time)
                    .and_modify(|queue| {
                        queue.pop_front();
                    });
            }
        }
    }

    pub fn active_actor(&self) -> Option<ActorKey> {
        self.actors_schedule
            .get(&self.current_time)
            .and_then(|queue| {
                queue
                    .iter()
                    .find(|&&actor_key| {
                        if let Ok(time) = self.actor_time(actor_key) {
                            time == self.current_time
                        } else {
                            false
                        }
                    })
                    .cloned()
            })
    }

    pub fn tick(&mut self) -> TimeResult<()> {
        if self.active_actor().is_none() {
            self.actors_schedule.remove(&self.current_time);
        } else {
            return Err(TimeError::ScheduleNotEmpty);
        }

        self.current_time += 1;
        Ok(())
    }
}
