use super::super::Corridor;
use crate::world::{Coord, CoordIndex, Dir};
use crate::{GameRng, WORLD_COLS, WORLD_ROWS};
// use rand::distributions::{Bernoulli, Distribution};
use rand::prelude::*;
use serde::{Deserialize, Serialize};

pub type Doors = Vec<(Dir, CoordIndex)>;

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct RoomPlan {
    pub north: CoordIndex,
    pub south: CoordIndex,
    pub east: CoordIndex,
    pub west: CoordIndex,
    pub doors: Doors,
}

impl RoomPlan {
    pub const ROOM_MIN_SIZE: CoordIndex = 7;
    pub const ROOM_MAX_SIZE: CoordIndex = 11;

    pub fn size(&self) -> CoordIndex {
        (self.south - self.north) * (self.east - self.west)
    }

    pub fn nw_corner(&self) -> Coord {
        Coord {
            row: self.north,
            col: self.west,
        }
    }

    // Returns the collection of `Coord`s belonging to the rooom.
    pub fn coords(&self) -> Vec<Coord> {
        let height = (self.south - self.north + 1) as usize;
        let width = (self.east - self.west + 1) as usize;
        let mut coords = Vec::with_capacity(height * width);

        for row in self.north..=self.south {
            for col in self.west..=self.east {
                coords.push(Coord { row, col });
            }
        }

        coords
    }

    pub fn center(&self) -> Coord {
        Coord {
            col: (self.east + self.west) / 2,
            row: (self.south + self.north) / 2,
        }
    }

    pub fn inside_coords(&self) -> Vec<Coord> {
        let height = (self.south - self.north - 1) as usize;
        let width = (self.east - self.west - 1) as usize;
        let mut coords = Vec::with_capacity(height * width);

        for row in (self.north + 1)..self.south {
            for col in (self.west + 1)..self.east {
                coords.push(Coord { row, col });
            }
        }

        coords
    }

    pub fn inner_coords(&self) -> Vec<Coord> {
        let height = (self.south - self.north - 3) as usize;
        let width = (self.east - self.west - 3) as usize;
        let mut coords = Vec::with_capacity(height * width);

        for row in (self.north + 2)..=(self.south - 2) {
            for col in (self.west + 2)..=(self.east - 2) {
                coords.push(Coord { row, col });
            }
        }

        coords
    }

    // pub fn border_coords_without_doors(&self) -> Vec<Coord> {
    //     let height = (self.south - self.north - 1) as usize;
    //     let width = (self.east - self.west - 1) as usize;
    //     let mut coords = Vec::with_capacity(2 * height + 2 * width - 4);

    //     for row in (self.north + 2)..=(self.south - 2) {
    //         if !self.doors.contains(&(Dir::West, row)) {
    //             coords.push(Coord {
    //                 row,
    //                 col: self.west + 1,
    //             });
    //         }
    //         if !self.doors.contains(&(Dir::East, row)) {
    //             coords.push(Coord {
    //                 row,
    //                 col: self.east - 1,
    //             });
    //         }
    //     }
    //     for col in (self.west + 2)..=(self.east - 2) {
    //         if !self.doors.contains(&(Dir::North, col)) {
    //             coords.push(Coord {
    //                 row: self.north + 1,
    //                 col,
    //             });
    //         }
    //         if !self.doors.contains(&(Dir::South, col)) {
    //             coords.push(Coord {
    //                 row: self.south - 1,
    //                 col,
    //             });
    //         }
    //     }
    //     if !self.doors.contains(&(Dir::North, self.west + 1))
    //         && !self.doors.contains(&(Dir::West, self.north + 1))
    //     {
    //         coords.push(Coord {
    //             row: self.north + 1,
    //             col: self.west + 1,
    //         });
    //     }
    //     if !self.doors.contains(&(Dir::North, self.east - 1))
    //         && !self.doors.contains(&(Dir::East, self.north + 1))
    //     {
    //         coords.push(Coord {
    //             row: self.north + 1,
    //             col: self.east - 1,
    //         });
    //     }
    //     if !self.doors.contains(&(Dir::South, self.west + 1))
    //         && !self.doors.contains(&(Dir::West, self.south - 1))
    //     {
    //         coords.push(Coord {
    //             row: self.south - 1,
    //             col: self.west + 1,
    //         });
    //     }
    //     if !self.doors.contains(&(Dir::South, self.east - 1))
    //         && !self.doors.contains(&(Dir::East, self.south - 1))
    //     {
    //         coords.push(Coord {
    //             row: self.south - 1,
    //             col: self.east - 1,
    //         });
    //     }

    //     coords
    // }

    pub fn border_coords(&self) -> Vec<Coord> {
        let height = (self.south - self.north - 1) as usize;
        let width = (self.east - self.west - 1) as usize;
        let mut coords = Vec::with_capacity(2 * height + 2 * width - 4);

        for row in self.north + 1..self.south {
            coords.push(Coord {
                row,
                col: self.west + 1,
            });
            coords.push(Coord {
                row,
                col: self.east - 1,
            });
        }
        for col in (self.west + 2)..self.east - 1 {
            coords.push(Coord {
                row: self.north + 1,
                col,
            });
            coords.push(Coord {
                row: self.south - 1,
                col,
            });
        }

        coords
    }

    pub fn wall_coords(&self) -> Vec<Coord> {
        let height = (self.south - self.north + 1) as usize;
        let width = (self.east - self.west + 1) as usize;
        let mut coords = Vec::with_capacity(2 * height + 2 * width - 4);

        for row in self.north..=self.south {
            if !self.doors.contains(&(Dir::West, row)) {
                coords.push(Coord {
                    row,
                    col: self.west,
                });
            }
            if !self.doors.contains(&(Dir::East, row)) {
                coords.push(Coord {
                    row,
                    col: self.east,
                });
            }
        }
        for col in (self.west + 1)..self.east {
            if !self.doors.contains(&(Dir::North, col)) {
                coords.push(Coord {
                    row: self.north,
                    col,
                });
            }
            if !self.doors.contains(&(Dir::South, col)) {
                coords.push(Coord {
                    row: self.south,
                    col,
                });
            }
        }

        coords
    }

    pub fn floor_coords(&self) -> Vec<Coord> {
        let height = (self.south - self.north - 3) as usize;
        let width = (self.east - self.west - 3) as usize;
        let mut coords = Vec::with_capacity(height * width);

        for row in (self.north + 1)..self.south {
            for col in (self.west + 1)..self.east {
                coords.push(Coord { row, col });
            }
        }

        coords
    }

    pub fn contains(&self, coord: Coord) -> bool {
        self.north <= coord.row
            && coord.row <= self.south
            && self.west <= coord.col
            && coord.col <= self.east
    }

    pub fn borders(&self, coord: Coord) -> bool {
        self.north - 1 <= coord.row
            && coord.row <= self.south + 1
            && self.west - 1 <= coord.col
            && coord.col <= self.east + 1
    }

    pub fn door_coord(&self, door_side: Dir, index: CoordIndex) -> Coord {
        match door_side {
            Dir::North => Coord {
                row: self.north,
                col: index,
            },
            Dir::South => Coord {
                row: self.south,
                col: index,
            },
            Dir::East => Coord {
                col: self.east,
                row: index,
            },
            Dir::West => Coord {
                col: self.west,
                row: index,
            },
        }
    }

    pub fn add_door(&mut self, coord: Coord) {
        if coord.row == self.north
            && self.west < coord.col
            && coord.col < self.east
            && !self.doors.contains(&(Dir::North, coord.col))
        {
            self.doors.push((Dir::North, coord.col));
        }
        if coord.row == self.south
            && self.west < coord.col
            && coord.col < self.east
            && !self.doors.contains(&(Dir::South, coord.col))
        {
            self.doors.push((Dir::South, coord.col));
        }
        if coord.col == self.west
            && self.north < coord.row
            && coord.row < self.south
            && !self.doors.contains(&(Dir::West, coord.row))
        {
            self.doors.push((Dir::West, coord.row));
        }
        if coord.col == self.east
            && self.north < coord.row
            && coord.row < self.south
            && !self.doors.contains(&(Dir::East, coord.row))
        {
            self.doors.push((Dir::East, coord.row));
        }
    }

    pub fn intersects(&self, other: &RoomPlan) -> bool {
        let delta_v = (other.south - other.north) / 2 + 1;
        let delta_h = (other.east - other.west) / 2 + 1;
        let plan = RoomPlan {
            north: self.north - delta_v,
            south: self.south + delta_v,
            west: self.west - delta_h,
            east: self.east + delta_h,
            doors: Vec::new(),
        };
        plan.contains(other.center())
    }

    pub fn split(&self, rng: &mut GameRng) -> Option<(Self, Self, Option<Corridor>)> {
        if rng.gen() {
            self.split_vertical(rng)
                .or_else(|| self.split_horizontal(rng))
        } else {
            self.split_horizontal(rng)
                .or_else(|| self.split_vertical(rng))
        }
    }

    fn split_horizontal(&self, rng: &mut GameRng) -> Option<(Self, Self, Option<Corridor>)> {
        let wall = (self.west + Self::ROOM_MIN_SIZE + 1..self.east - Self::ROOM_MIN_SIZE)
            .filter(|&col| col % 2 == 1)
            .choose(rng)?;

        let mut west_doors: Doors;
        let mut east_doors: Doors;
        let offset: CoordIndex;
        let opt_corridor: Option<Corridor>;
        let wall_length = self.south - self.north + 1;

        if wall_length > 2 * Self::ROOM_MAX_SIZE {
            west_doors = self
                .doors
                .iter()
                .filter(|&&(dir, index)| match dir {
                    Dir::West => true,
                    Dir::East => false,
                    _ => index < wall,
                })
                .cloned()
                .collect();
            east_doors = self
                .doors
                .iter()
                .filter(|&&(dir, index)| match dir {
                    Dir::West => false,
                    Dir::East => true,
                    _ => index > wall,
                })
                .cloned()
                .collect();

            let west_door = rng.gen_range(self.north + 2..self.south - 1) / 2 * 2 + 1; // enforce parity
            west_doors.push((Dir::East, west_door));
            let east_door = rng.gen_range(self.north + 2..self.south - 1) / 2 * 2 + 1; // enforce parity
            east_doors.push((Dir::West, east_door));

            offset = 1;
            opt_corridor = Some(Corridor {
                start: Coord {
                    row: self.north.max(1), // keep outer wall
                    col: wall,
                },
                dir: Dir::South,
                length: (self.south - self.north.max(1) + 1)
                    .min(WORLD_ROWS as CoordIndex - self.north.max(1) - 1),
            });
        } else {
            west_doors = self
                .doors
                .iter()
                .filter(|&&(dir, index)| match dir {
                    Dir::West => true,
                    Dir::East => false,
                    _ => index < wall,
                })
                .cloned()
                .collect();
            east_doors = self
                .doors
                .iter()
                .filter(|&&(dir, index)| match dir {
                    Dir::West => false,
                    Dir::East => true,
                    _ => index >= wall,
                })
                .cloned()
                .collect();

            let door = rng.gen_range(self.north + 2..self.south - 1) / 2 * 2 + 1; // enforce parity
            west_doors.push((Dir::East, door));
            east_doors.push((Dir::West, door));

            offset = 0;
            opt_corridor = None;
        }

        let west_room = RoomPlan {
            north: self.north,
            south: self.south,
            east: wall - 1,
            west: self.west,
            doors: west_doors,
        };

        let east_room = RoomPlan {
            north: self.north,
            south: self.south,
            east: self.east,
            west: wall - 1 + 2 * offset,
            doors: east_doors,
        };

        Some((west_room, east_room, opt_corridor))
    }

    fn split_vertical(&self, rng: &mut GameRng) -> Option<(Self, Self, Option<Corridor>)> {
        let wall = (self.north + Self::ROOM_MIN_SIZE + 1..self.south - Self::ROOM_MIN_SIZE)
            .filter(|&row| row % 2 == 1)
            .choose(rng)?;

        let mut north_doors: Doors;
        let mut south_doors: Doors;
        let offset: CoordIndex;
        let opt_corridor: Option<Corridor>;
        let wall_length = self.east - self.west + 1;

        if wall_length > 2 * Self::ROOM_MAX_SIZE {
            north_doors = self
                .doors
                .iter()
                .filter(|&&(dir, index)| match dir {
                    Dir::North => true,
                    Dir::South => false,
                    _ => index < wall,
                })
                .cloned()
                .collect();
            south_doors = self
                .doors
                .iter()
                .filter(|&&(dir, index)| match dir {
                    Dir::North => false,
                    Dir::South => true,
                    _ => index > wall,
                })
                .cloned()
                .collect();

            let north_door = rng.gen_range(self.west + 2..self.east - 1) / 2 * 2 + 1; // enforce parity
            north_doors.push((Dir::South, north_door));
            let south_door = rng.gen_range(self.west + 2..self.east - 1) / 2 * 2 + 1; // enforce parity
            south_doors.push((Dir::North, south_door));

            offset = 1;
            opt_corridor = Some(Corridor {
                start: Coord {
                    row: wall,
                    col: self.west.max(1),
                },
                dir: Dir::East,
                length: (self.east - self.west.max(1) + 1)
                    .min(WORLD_COLS as CoordIndex - self.west.max(1) - 1),
            });
        } else {
            north_doors = self
                .doors
                .iter()
                .filter(|&&(dir, index)| match dir {
                    Dir::North => true,
                    Dir::South => false,
                    _ => index < wall,
                })
                .cloned()
                .collect();
            south_doors = self
                .doors
                .iter()
                .filter(|&&(dir, index)| match dir {
                    Dir::North => false,
                    Dir::South => true,
                    _ => index >= wall,
                })
                .cloned()
                .collect();

            let door = rng.gen_range(self.west + 2..self.east - 1) / 2 * 2 + 1; // enforce parity
            north_doors.push((Dir::South, door));
            south_doors.push((Dir::North, door));

            offset = 0;
            opt_corridor = None;
        }

        let north_room = RoomPlan {
            north: self.north,
            south: wall - 1,
            east: self.east,
            west: self.west,
            doors: north_doors,
        };

        let south_room = RoomPlan {
            north: wall - 1 + 2 * offset,
            south: self.south,
            east: self.east,
            west: self.west,
            doors: south_doors,
        };

        Some((north_room, south_room, opt_corridor))
    }
}
