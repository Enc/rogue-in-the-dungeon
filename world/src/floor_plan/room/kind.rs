use crate::actor::ActorKind;
use crate::item::{Armor, ItemKind, Potion, Scroll, Wand, Weapon};
use crate::tile::{FloorKind, TileKind, WallKind};
use serde::{Deserialize, Serialize};

#[derive(Clone, Copy, Debug, Serialize, Deserialize)]
pub enum RoomType {
    Plain,
    Barracks,
    Vaults,
    Chasm,
    Stairs,
    Landing,
    Temple,
}

impl RoomType {
    pub fn floor(self) -> FloorKind {
        match self {
            RoomType::Plain
            | RoomType::Vaults
            | RoomType::Chasm
            | RoomType::Stairs
            | RoomType::Landing => FloorKind::Soil,
            RoomType::Barracks => FloorKind::Plank,
            RoomType::Temple => FloorKind::Stone,
        }
    }

    pub fn wall(self) -> WallKind {
        match self {
            RoomType::Plain
            | RoomType::Vaults
            | RoomType::Chasm
            | RoomType::Stairs
            | RoomType::Landing
            | RoomType::Temple => WallKind::Stone,
            RoomType::Barracks => WallKind::Brick,
        }
    }

    pub fn mob_kind(self) -> &'static [ActorKind] {
        match self {
            RoomType::Plain => &[ActorKind::Rat],
            RoomType::Vaults => &[ActorKind::Rat, ActorKind::Zombie],
            RoomType::Barracks => &[ActorKind::Ork, ActorKind::Goblin, ActorKind::Troll],
            RoomType::Chasm => &[ActorKind::Bat],
            _ => &[],
        }
    }

    pub fn item_kind(self) -> &'static [ItemKind] {
        match self {
            RoomType::Plain => &[ItemKind::Wand(Wand::Lightning)],
            RoomType::Vaults => &[ItemKind::Potion(Potion::Health)],
            RoomType::Barracks => &[
                ItemKind::Armor(Armor::Leather),
                ItemKind::Armor(Armor::Chainmail),
                ItemKind::Armor(Armor::Plate),
                ItemKind::Weapon(Weapon::Dagger),
                ItemKind::Weapon(Weapon::Sword),
                ItemKind::Weapon(Weapon::WarHammer),
            ],
            _ => &[],
        }
    }
}
