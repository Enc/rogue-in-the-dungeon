mod kind;
mod plan;

pub use self::kind::RoomType;
pub use self::plan::{Doors, RoomPlan};

use crate::world::CoordIndex;
use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Room {
    pub room_type: RoomType,
    pub plan: RoomPlan,
}

impl Room {
    pub fn size(&self) -> CoordIndex {
        self.plan.size()
    }
}

// impl Room {
//     pub fn build(&self, floor: CoordIndex, world_map: &mut WorldMap, rng: &mut GameRng) {
//         let plan = &self.plan;

//         let wall_type = self.room_type.wall();
//         let floor_type = self.room_type.floor();
//         for row in plan.north..=plan.south {
//             for col in plan.west..=plan.east {
//                 let coord = Coord { row, col, floor };
//                 if col == plan.west && !plan.doors.contains(&(Dir::West, row))
//                     || col == plan.east && !plan.doors.contains(&(Dir::East, row))
//                     || row == plan.north && !plan.doors.contains(&(Dir::North, col))
//                     || row == plan.south && !plan.doors.contains(&(Dir::South, col))
//                 {
//                     world_map.set_tile(coord, Tile::new(wall_type));
//                 } else {
//                     world_map.set_tile(coord, Tile::new(floor_type));
//                 }
//             }
//         }

//         self.furnish_room(floor, world_map, rng);
//         // self.populate_room(room, world, rng);
//     }

//     fn furnish_room(&self, floor: CoordIndex, world_map: &mut WorldMap, rng: &mut GameRng) {
//         let mut inner_coord = self.plan.inner_coords();
//         let mut border_coord = self.plan.border_coords();
//         let mut wall_coord = self.plan.wall_coords();
//         inner_coord.shuffle(rng);
//         border_coord.shuffle(rng);
//         wall_coord.shuffle(rng);

//         // 'ent: for (item_builder, position, min, max) in self.room_type.items() {
//         //     for _ in 0..rng.gen_range(min, max + 1) {
//         //         if let Some(coord) = match position {
//         //             RoomPosition::Inner => inner_coord.pop(),
//         //             RoomPosition::Border => border_coord.pop(),
//         //             RoomPosition::Wall => wall_coord.pop(),
//         //         } {
//         //             // let id = self.build_entity(name, world, rng);
//         //             // world.place(id, coord).expect("place entity");
//         //             // let item_builder = ItemBuilder { kind };
//         //             let coord = Coord {
//         //                 row: coord.row,
//         //                 col: coord.col,
//         //                 floor,
//         //             };
//         //             // let item = item_builder.clone().build();
//         //             // world_map
//         //             //     .insert_item(item, ItemLocation::OnMap(coord))
//         //             //     .expect("insert item");
//         //             unimplemented!();
//         //         } else {
//         //             continue 'ent;
//         //         }
//         //     }
//         // }

//         'ent_ftrs: for (feature, position, min, max) in self.room_type.features() {
//             for _ in 0..rng.gen_range(min, max + 1) {
//                 if let Some(coord) = match position {
//                     RoomPosition::Inner => inner_coord.pop(),
//                     RoomPosition::Border => border_coord.pop(),
//                     RoomPosition::Wall => wall_coord.pop(),
//                 } {
//                     // let id = self.build_entity(name, world, rng);
//                     // world.place(id, coord).expect("place entity");
//                     // let item_builder = ItemBuilder { kind };
//                     let coord = Coord {
//                         row: coord.row,
//                         col: coord.col,
//                         floor,
//                     };
//                     world_map
//                         .with_feature(coord, Some(feature))
//                         .expect("insert feature");
//                 // .insert_item(item, ItemLocation::OnMap(coord))
//                 } else {
//                     continue 'ent_ftrs;
//                 }
//             }
//         }
//     }
// }
