use crate::world::{Coord, CoordIndex, Dir};
use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Corridor {
    pub start: Coord,
    pub dir: Dir,
    pub length: CoordIndex,
}

impl Corridor {
    pub fn end(&self) -> Coord {
        self.start
            + match self.dir {
                Dir::North => Coord {
                    row: -self.length + 1,
                    col: 0,
                },
                Dir::South => Coord {
                    row: self.length - 1,
                    col: 0,
                },
                Dir::West => Coord {
                    row: 0,
                    col: -self.length + 1,
                },
                Dir::East => Coord {
                    row: 0,
                    col: self.length - 1,
                },
            }
    }

    pub fn contains(&self, coord: Coord) -> bool {
        let dir_check = match self.dir {
            Dir::North | Dir::South => self.start.col == coord.col,
            Dir::East | Dir::West => self.start.row == coord.row,
        };
        let length_check = match self.dir {
            Dir::North => coord.row <= self.start.row && self.start.row <= coord.row + self.length,
            Dir::South => coord.row >= self.start.row && self.start.row >= coord.row + self.length,
            Dir::West => coord.col <= self.start.col && self.start.col <= coord.col + self.length,
            Dir::East => coord.col >= self.start.col && self.start.col >= coord.col + self.length,
        };
        dir_check && length_check
    }
}
