#![forbid(unsafe_code)]

mod actor;
mod floor_plan;
mod item;
mod tile;
mod time;
mod world;

use rand_xoshiro::Xoshiro256StarStar;
use slotmap::new_key_type;
use std::collections::{HashMap, HashSet};

pub use actor::{
    Actor, ActorError, ActorKind, Alignment, Damage, DamageKind, Element, PhysicalDamage, Stat,
    STATS,
};
pub use item::{Armor, Item, ItemKind, Potion, Scroll, Wand, Weapon};
pub use tile::{Feature, FloorKind, Tile, TileKind, WallKind};
pub use time::{TimeError, TimeResult, TimeUnit};
pub use world::{Coord, Dir, ItemCoord, World, WorldError, WorldResult, DIRS};

/// The chosen RNG type.
pub type GameRng = Xoshiro256StarStar;

/// The seed for the RNG.
pub type GameSeed = [u8; 32];

new_key_type! {
    /// The key type for `Actor`s stored in a `SlotMap`.
    pub struct ActorKey;

    /// The key type for `Item`s stored in a `SlotMap`.
    pub struct ItemKey;
}

/// The chosen map type.
pub type Map<K, V> = HashMap<K, V>;

/// The chosen set type.
pub type Set<T> = HashSet<T>;

/// The vertical size of the world map.
pub const WORLD_ROWS: usize = 35;

/// The horizontal size of the world map.
pub const WORLD_COLS: usize = 75;

/// The size of the world.
pub const WORLD_DIM: [usize; 2] = [WORLD_ROWS, WORLD_COLS];

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
