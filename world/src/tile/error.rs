use std::error::Error;
use std::fmt;

#[derive(Debug)]
pub enum TileError {
    NoLight,
    ATileError,
}

impl fmt::Display for TileError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let error_msg = match self {
            _ => "a tile error",
        };
        write!(f, "{}", error_msg)
    }
}

impl Error for TileError {}

pub type TileResult<T> = Result<T, TileError>;
