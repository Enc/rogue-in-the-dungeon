use serde::{Deserialize, Serialize};

#[derive(Clone, Copy, Debug, Serialize, Deserialize)]
pub enum Feature {
    Altar { used: bool },
    Blood,
    Door { open: bool },
    Fire { fuel: u8 },
    Flooded,
    Vegetation { trampled: bool },
}

impl Feature {
    pub fn impassable(self) -> bool {
        match self {
            Feature::Door { open } => !open,
            Feature::Altar { .. } => true,
            _ => false,
        }
    }

    pub fn steppable(self) -> bool {
        match self {
            Feature::Door { open } => open,
            _ => true,
        }
    }

    pub fn opaque(self) -> bool {
        match self {
            Feature::Door { open } => !open,
            Feature::Vegetation { trampled } => !trampled,
            _ => false,
        }
    }

    pub fn flammable(self) -> Option<u8> {
        match self {
            Feature::Door { .. } => Some(12),
            Feature::Vegetation { .. } => Some(8),
            _ => None,
        }
    }
}
