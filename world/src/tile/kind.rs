use serde::{Deserialize, Serialize};

#[derive(Clone, Copy, Debug, Serialize, Deserialize)]
pub enum FloorKind {
    Plank,
    Soil,
    Stone,
}

impl FloorKind {
    pub fn is_fertile(&self) -> bool {
        matches!(self, FloorKind::Soil)
    }
}

#[derive(Clone, Copy, Debug, Serialize, Deserialize)]
pub enum WallKind {
    Brick,
    Stone,
    Molded,
}

#[derive(Clone, Copy, Debug, Serialize, Deserialize)]
pub enum TileKind {
    Chasm,
    Stairs,
    Floor(FloorKind),
    Wall(WallKind),
}

impl TileKind {
    pub fn impassable(self) -> bool {
        matches!(self, TileKind::Wall(_))
    }

    pub fn steppable(self) -> bool {
        matches!(self, TileKind::Floor(_) | TileKind::Stairs)
    }

    pub fn opaque(self) -> bool {
        matches!(self, TileKind::Wall(_))
    }

    pub fn is_fertile(&self) -> bool {
        matches!(self, TileKind::Floor(floor_kind) if floor_kind.is_fertile())
    }
}
