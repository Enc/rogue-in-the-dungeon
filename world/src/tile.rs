mod error;
mod feature;
mod kind;

pub use error::{TileError, TileResult};
pub use feature::Feature;
pub use kind::{FloorKind, TileKind, WallKind};
use serde::{Deserialize, Serialize};

#[derive(Clone, Copy, Debug, Serialize, Deserialize)]
pub struct Tile {
    kind: TileKind,
    feature: Option<Feature>,
}

impl Tile {
    pub fn new(kind: TileKind) -> Self {
        Self {
            kind,
            feature: None,
        }
    }

    pub fn new_floor(floor_kind: FloorKind) -> Self {
        Self::new(TileKind::Floor(floor_kind))
    }

    pub fn new_wall(wall_kind: WallKind) -> Self {
        Self::new(TileKind::Wall(wall_kind))
    }

    pub fn kind(&self) -> TileKind {
        self.kind
    }

    pub fn feature(&self) -> Option<Feature> {
        self.feature
    }

    pub fn set_kind(&mut self, kind: TileKind) {
        self.kind = kind;
    }

    pub fn with_feature(&mut self, feature: Option<Feature>) {
        self.feature = feature;
    }

    pub fn impassable(&self) -> bool {
        self.kind.impassable() || self.feature.map(Feature::impassable).unwrap_or(false)
    }

    pub fn steppable(&self) -> bool {
        !self.impassable()
            && self.kind.steppable()
            && self.feature.map(Feature::steppable).unwrap_or(true)
    }

    pub fn opaque(&self) -> bool {
        self.kind.opaque() || self.feature.map(Feature::opaque).unwrap_or(false)
    }
}
