mod damage;
mod error;
mod kind;

pub use damage::{Damage, DamageKind, Element, PhysicalDamage};
pub use error::{ActorError, ActorResult};
pub use kind::ActorKind;

use crate::GameRng;
use crate::{Item, ItemKey, ItemKind};
use rand::prelude::*;
use serde::{Deserialize, Serialize};
use slotmap::SlotMap;

#[derive(Clone, Copy, Debug, Serialize, Deserialize)]
pub enum Alignment {
    Player,
    Dungeon,
}

#[derive(Clone, Copy, Debug, Serialize, Deserialize)]
pub enum Stat {
    Con,
    Dex,
    Int,
    Per,
    Str,
}

pub const STATS: [Stat; 5] = [Stat::Con, Stat::Dex, Stat::Int, Stat::Per, Stat::Str];

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Actor {
    alignment: Alignment,
    kind: ActorKind,
    items: SlotMap<ItemKey, Item>,
    equipped_weapon: Option<ItemKey>,
    equipped_armor: Option<ItemKey>,
    health_points: u8,
    constitution: u8,
    strength: u8,
    dexterity: u8,
    perception: u8,
    intelligence: u8,
    burning: u8,
    bleeding: u8,
}

impl Actor {
    pub fn generate(kind: ActorKind, alignment: Alignment, rng: &mut GameRng) -> Self {
        let mut actor = Actor {
            alignment,
            kind,
            items: SlotMap::default(),
            equipped_weapon: None,
            equipped_armor: None,
            health_points: 0,
            constitution: kind.constitution(),
            strength: kind.strength(),
            dexterity: kind.dexterity(),
            perception: kind.perception(),
            intelligence: kind.intelligence(),
            burning: 0,
            bleeding: 0,
        };
        actor.health_points = actor.max_health_points();
        actor.outfit(rng).expect("Equip actor");
        actor
    }

    pub fn alignment(&self) -> Alignment {
        self.alignment
    }

    pub fn kind(&self) -> ActorKind {
        self.kind
    }

    pub fn constitution(&self) -> u8 {
        self.constitution
    }

    pub fn strength(&self) -> u8 {
        self.strength
    }

    pub fn dexterity(&self) -> u8 {
        self.dexterity
    }

    pub fn perception(&self) -> u8 {
        self.perception
    }

    pub fn intelligence(&self) -> u8 {
        self.intelligence
    }

    pub fn increase_stat(&mut self, stat: Stat) {
        match stat {
            Stat::Con => self.constitution += 1,
            Stat::Dex => self.dexterity += 1,
            Stat::Int => self.intelligence += 1,
            Stat::Per => self.perception += 1,
            Stat::Str => self.strength += 1,
        }
    }

    pub fn health_points(&self) -> u8 {
        self.health_points
    }

    pub fn max_health_points(&self) -> u8 {
        self.constitution() * 3
    }

    pub fn take_damage(&mut self, damage: Damage) {
        match damage.kind {
            DamageKind::Elemental(Element::Fire) => self.burning = self.burning.max(damage.amount),
            DamageKind::Physical(PhysicalDamage::Slashing) => self.bleeding += damage.amount,
            _ => self.health_points = self.health_points.saturating_sub(damage.amount),
        }
        // match damage.kind {
        //     DamageKind::Elemental(element) => match element {
        //         Element::Fire => self.burning = self.burning.max(damage.amount),
        //         _ => self.health_points = self.health_points.saturating_sub(damage.amount),
        //     }
        //     DamageKind::Physical(physical) => match physical {
        //         PhysicalDamage::Slashing => self.bleeding += damage.amount,
        //         _ => self.health_points = self.health_points.saturating_sub(damage.amount),
        //     }
        // }
    }

    pub fn heal(&mut self) {
        self.health_points = self.max_health_points();
    }

    pub fn hit_roll(&self, rng: &mut GameRng) -> u8 {
        rng.gen_range(1..=self.dexterity())
    }

    pub fn dodge_roll(&self, rng: &mut GameRng) -> u8 {
        rng.gen_range(1..=self.dexterity())
    }

    pub fn damage_roll(&self, rng: &mut GameRng) -> Damage {
        let kind;
        let mult;
        if let Some(item_key) = self.equipped_weapon {
            if let ItemKind::Weapon(weapon) =
                self.items.get(item_key).expect("equipped weapon").kind()
            {
                kind = weapon.damage();
                mult = weapon.damage_mult();
            } else {
                panic!("equipped item is not a weapon");
            }
        } else {
            kind = DamageKind::Physical(PhysicalDamage::Bludgeoning);
            mult = 1;
        }
        let amount = rng.gen_range(1..=self.strength().min(mult as u8));
        Damage { amount, kind }
        // (1..mult).map(|_| rng.gen_range(1..=self.strength().min(kind))).sum()
        // Die(self.strength()).roll(mult, rng);
        // rng.gen_range(1..=self.strength())
    }

    pub fn armor_roll(&self, damage_kind: DamageKind, rng: &mut GameRng) -> u8 {
        let defense = match damage_kind {
            DamageKind::Physical(PhysicalDamage::Piercing) | DamageKind::Elemental(_) => 0,
            DamageKind::Physical(PhysicalDamage::Bludgeoning)
            | DamageKind::Physical(PhysicalDamage::Slashing) => {
                if let Some(item_key) = self.equipped_armor() {
                    if let ItemKind::Armor(armor) =
                        self.item(item_key).expect("equipped armor").kind()
                    {
                        // damage_absorbed = Die(armor.defense()).roll(1, &mut self.rng);
                        armor.defense()
                    } else {
                        panic!("Worn item is no armor");
                        // return Err(GameError::from(WorldError::NotAnArmor(item_key)));
                    }
                } else {
                    0
                }
            }
        };
        rng.gen_range(0..=defense) // Should range start from 0?
    }

    pub fn sight_range(&self) -> u8 {
        self.perception().max(1)
    }

    pub fn burning(&self) -> u8 {
        self.burning
    }

    pub fn burn(&mut self) {
        const BURNING_SPEED: u8 = 8;
        let burning = (self.burning + BURNING_SPEED) / (BURNING_SPEED + 1);
        self.burning -= burning;
        self.health_points = self.health_points.saturating_sub(burning);
    }

    pub fn bleeding(&self) -> u8 {
        self.bleeding
    }

    pub fn bleed(&mut self) {
        const BLEEDING_SPEED: u8 = 8;
        let bleeding = (self.bleeding + BLEEDING_SPEED) / (BLEEDING_SPEED + 1);
        self.bleeding -= bleeding;
        self.health_points = self.health_points.saturating_sub(bleeding);
    }

    pub fn items(&self) -> &SlotMap<ItemKey, Item> {
        &self.items
    }

    pub fn item(&self, item_key: ItemKey) -> ActorResult<&Item> {
        self.items
            .get(item_key)
            .ok_or(ActorError::NotInInventory(item_key))
    }

    pub fn has_item(&self, item_key: ItemKey) -> ActorResult<()> {
        if self.items.contains_key(item_key) {
            Ok(())
        } else {
            Err(ActorError::NotInInventory(item_key))
        }
    }

    pub fn take_item(&mut self, item: Item) -> ActorResult<ItemKey> {
        if self.items.len() < self.strength() as usize {
            Ok(self.items.insert(item))
        } else {
            todo!()
        }
    }

    pub fn drop_item(&mut self, item_key: ItemKey) -> ActorResult<Item> {
        if let Some(armor_key) = self.equipped_armor {
            if armor_key == item_key {
                let _ = self.unequip_armor()?;
            }
        }
        if let Some(weapon_key) = self.equipped_weapon {
            if weapon_key == item_key {
                let _ = self.unequip_weapon()?;
            }
        }
        self.items
            .remove(item_key)
            .ok_or(ActorError::NotInInventory(item_key))
    }

    pub fn equipped_armor(&self) -> Option<ItemKey> {
        self.equipped_armor
    }

    pub fn equip_armor(&mut self, item_key: ItemKey) -> ActorResult<()> {
        if let Some(item) = self.items.get(item_key) {
            if let ItemKind::Armor(_) = item.kind() {
                self.equipped_armor = Some(item_key);
                Ok(())
            } else {
                Err(ActorError::NotAnArmor(item_key))
            }
        } else {
            Err(ActorError::NotInInventory(item_key))
        }
    }

    pub fn unequip_armor(&mut self) -> ActorResult<ItemKey> {
        if let Some(item_key) = self.equipped_armor {
            self.equipped_armor = None;
            Ok(item_key)
        } else {
            Err(ActorError::NoEquippedArmor)
        }
    }

    pub fn equipped_weapon(&self) -> Option<ItemKey> {
        self.equipped_weapon
    }

    pub fn equip_weapon(&mut self, item_key: ItemKey) -> ActorResult<()> {
        if let Some(item) = self.items.get(item_key) {
            if let ItemKind::Weapon(_) = item.kind() {
                self.equipped_weapon = Some(item_key);
                Ok(())
            } else {
                Err(ActorError::NotAWeapon(item_key))
            }
        } else {
            Err(ActorError::NotInInventory(item_key))
        }
    }

    pub fn unequip_weapon(&mut self) -> ActorResult<ItemKey> {
        if let Some(item_key) = self.equipped_weapon {
            self.equipped_weapon = None;
            Ok(item_key)
        } else {
            Err(ActorError::NoEquippedWeapon)
        }
    }

    fn outfit(&mut self, rng: &mut GameRng) -> ActorResult<()> {
        for &(item_kind, prob) in self.kind.equipment() {
            if rng.gen_bool(prob) {
                let item = Item::new(item_kind);
                self.take_item(item)?;
            }
        }

        if let Some(armor_kind) = self.kind.armor() {
            let armor = Item::new(ItemKind::Armor(armor_kind));
            let armor_key = self.take_item(armor)?;
            self.equip_armor(armor_key)?;
        }

        if let Some(weapon_kind) = self.kind.weapon() {
            let weapon = Item::new(ItemKind::Weapon(weapon_kind));
            let weapon_key = self.take_item(weapon)?;
            self.equip_weapon(weapon_key)?;
        }

        Ok(())
    }
}
