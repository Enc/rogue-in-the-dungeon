use crate::item::{Armor, ItemKind, Potion, Scroll, Wand, Weapon};
use serde::{Deserialize, Serialize};

#[derive(Clone, Copy, Debug, Serialize, Deserialize)]
pub enum ActorKind {
    Player,
    Bat,
    Rat,
    Goblin,
    Ork,
    Troll,
    Zombie,
}

impl ActorKind {
    const PLAYER_STARTING_STAT: u8 = 6;

    pub fn strength(&self) -> u8 {
        match self {
            ActorKind::Player => Self::PLAYER_STARTING_STAT,
            ActorKind::Goblin => 5,
            ActorKind::Zombie | ActorKind::Ork => 6,
            ActorKind::Troll => 8,
            ActorKind::Bat | ActorKind::Rat => 4,
        }
    }

    pub fn constitution(&self) -> u8 {
        match self {
            ActorKind::Player => Self::PLAYER_STARTING_STAT,
            ActorKind::Goblin => 5,
            ActorKind::Zombie | ActorKind::Ork => 6,
            ActorKind::Troll => 8,
            ActorKind::Bat | ActorKind::Rat => 4,
        }
    }

    pub fn dexterity(&self) -> u8 {
        match self {
            ActorKind::Player => Self::PLAYER_STARTING_STAT,
            ActorKind::Goblin | ActorKind::Ork => 5,
            ActorKind::Zombie | ActorKind::Troll => 4,
            ActorKind::Bat | ActorKind::Rat => 6,
        }
    }

    pub fn perception(&self) -> u8 {
        match self {
            ActorKind::Player => Self::PLAYER_STARTING_STAT,
            ActorKind::Rat => 6,
            ActorKind::Zombie | ActorKind::Troll => 4,
            ActorKind::Bat | ActorKind::Ork | ActorKind::Goblin => 5,
        }
    }

    pub fn intelligence(&self) -> u8 {
        match self {
            ActorKind::Player => Self::PLAYER_STARTING_STAT,
            ActorKind::Rat | ActorKind::Zombie | ActorKind::Troll => 2,
            ActorKind::Bat | ActorKind::Ork | ActorKind::Goblin => 4,
        }
    }

    pub fn flying(&self) -> bool {
        matches!(self, ActorKind::Bat)
    }

    pub fn occurrence_prob(self, level: u8) -> u8 {
        const BASE_PROB: u8 = 5;

        match self {
            ActorKind::Bat | ActorKind::Rat => 2 * BASE_PROB,
            ActorKind::Zombie => 2 * level,
            ActorKind::Troll => level,
            ActorKind::Ork | ActorKind::Goblin => BASE_PROB + level,
            ActorKind::Player => 0,
        }
    }

    pub fn equipment(self) -> &'static [(ItemKind, f64)] {
        match self {
            ActorKind::Player => &[
                (ItemKind::Potion(Potion::Health), 1.0),
                (ItemKind::Wand(Wand::FireBolt), 1.0),
            ],
            ActorKind::Ork => &[(ItemKind::Potion(Potion::Health), 0.3)],
            ActorKind::Goblin => &[
                (ItemKind::Potion(Potion::Health), 0.3),
                (ItemKind::Wand(Wand::Lightning), 0.3),
            ],
            ActorKind::Zombie => &[(ItemKind::Wand(Wand::Lightning), 0.5)],
            _ => &[],
        }
    }

    pub fn armor(self) -> Option<Armor> {
        match self {
            ActorKind::Ork => Some(Armor::Leather),
            ActorKind::Goblin => Some(Armor::Chainmail),
            _ => None,
        }
    }

    pub fn weapon(self) -> Option<Weapon> {
        match self {
            ActorKind::Player => Some(Weapon::Sword),
            ActorKind::Ork => Some(Weapon::Sword),
            ActorKind::Goblin => Some(Weapon::Dagger),
            ActorKind::Troll => Some(Weapon::WarHammer),
            _ => None,
        }
    }
}
