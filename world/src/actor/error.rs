use crate::ItemKey;
use std::error::Error;
use std::fmt;

#[derive(Debug)]
pub enum ActorError {
    EquippedArmor(ItemKey),
    EquippedWeapon(ItemKey),
    InventoryFull,
    NoEquippedArmor,
    NoEquippedWeapon,
    NotInInventory(ItemKey),
    NotAnArmor(ItemKey),
    NotAWeapon(ItemKey),
}

impl fmt::Display for ActorError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let error_msg = match self {
            _ => "an actor error",
        };
        write!(f, "{}", error_msg)
    }
}

impl Error for ActorError {}

pub type ActorResult<T> = Result<T, ActorError>;
