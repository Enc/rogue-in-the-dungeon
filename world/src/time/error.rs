use super::TimeUnit;
use crate::ActorKey;
use std::error::Error;
use std::fmt;

#[derive(Clone, Debug)]
pub enum TimeError {
    AlreadyScheduled { actor_key: ActorKey, time: TimeUnit },
    NotScheduled(ActorKey),
    NullSchedule(ActorKey),
    ScheduleNotEmpty,
}

impl fmt::Display for TimeError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            _ => write!(f, "a time error"),
        }
    }
}

impl Error for TimeError {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        match self {
            _ => None,
        }
    }
}

pub type TimeResult<T> = Result<T, TimeError>;
