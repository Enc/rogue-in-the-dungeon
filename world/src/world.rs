mod error;
mod grid;
mod spt_map;
mod tile_map;

pub use error::{WorldError, WorldResult};
pub use grid::{Coord, CoordIndex, Dir, ItemCoord, DIRS};
use spt_map::SptMap;
use tile_map::TileMap;

use crate::actor::{Actor, Alignment};
use crate::floor_plan::{FloorPlan, Room};
use crate::item::Item;
use crate::tile::Feature;
use crate::time::{Time, TimeUnit};
use crate::{ActorKey, GameRng, ItemKey, WORLD_DIM};
use ndarray::{Array2, Ix2, Zip};
use rand::prelude::*;
use rand::Rng;
use serde::{Deserialize, Serialize};
use slotmap::{DenseSlotMap, SecondaryMap, SlotMap, SparseSecondaryMap};

/// `World` contains and manages the game world's data.
#[derive(Serialize, Deserialize)]
pub struct World {
    time: Time,
    tile_map: TileMap,
    floor_plan: FloorPlan,
    items: SlotMap<ItemKey, Item>,
    items_map: SptMap<ItemCoord, ItemKey>,
    actors: DenseSlotMap<ActorKey, Actor>,
    actors_map: SptMap<Coord, ActorKey>,
    sight_map: SecondaryMap<ActorKey, Array2<bool>>,
    memory_map: SparseSecondaryMap<ActorKey, Array2<bool>>,
}

impl World {
    pub fn generate(floor: CoordIndex, rng: &mut GameRng) -> World {
        let floor_plan = FloorPlan::generate_dungeon(floor, 2, rng);

        let mut world = World {
            time: Time::default(),
            tile_map: TileMap::generate(&floor_plan, rng),
            floor_plan,
            items: SlotMap::default(),
            items_map: SptMap::default(),
            actors: DenseSlotMap::default(),
            actors_map: SptMap::default(),
            sight_map: SecondaryMap::default(),
            memory_map: SparseSecondaryMap::default(),
        };

        world.populate(rng);
        world.furnish(rng);

        world
    }

    /// Generate a new floor.
    /// Clean up old data, but keep the player actor (and possibly its allies).
    pub fn generate_new_floor(&mut self, rng: &mut GameRng) {
        // Generate new floor plan
        let floor = self.floor_plan.floor + 1;
        let floor_plan = FloorPlan::generate_dungeon(floor, 2, rng);
        self.floor_plan = floor_plan;

        // Generate new tilemap
        self.tile_map = TileMap::generate(&self.floor_plan, rng);

        let landing = self.floor_plan.landing;
        let landing_room = self
            .floor_plan
            .find_room(landing)
            .expect("landing room")
            .clone();
        let actor_keys: Vec<ActorKey> = self.actors.keys().collect();
        for actor_key in actor_keys.into_iter() {
            if let Alignment::Player = self.actor(actor_key).expect("actor").alignment() {
                // Place player actor(s) in the landing room
                let landing_coord = self
                    .choose_actor_coord(&landing_room, rng)
                    .expect("landing coord");
                self.move_actor(actor_key, landing_coord)
                    .expect("insert player to landing");
                // Refresh memory map
                self.add_memory_map(actor_key);
            } else {
                // Remove all non-player actors
                let _ = self.remove_actor(actor_key).expect("delete actor");
            }
        }

        self.populate(rng);
        self.furnish(rng);
    }

    /// Populate the floor with mobs.
    fn populate(&mut self, rng: &mut GameRng) {
        let plan = self.floor_plan.clone();
        let level = plan.floor as u8;

        for room in plan.rooms {
            let doors = room.plan.doors.len();
            let (min, max) = (doors.saturating_sub(2), doors);
            for _ in 0..rng.gen_range(min..=max) {
                if let Some(coord) = self.choose_actor_coord(&room, rng) {
                    if let Ok(&actor_kind) = room
                        .room_type
                        .mob_kind()
                        .choose_weighted(rng, |actor_kind| actor_kind.occurrence_prob(level))
                    {
                        let actor = Actor::generate(actor_kind, Alignment::Dungeon, rng);
                        let _ = self.insert_actor(actor, coord).expect("add actor");
                    }
                }
            }
        }
    }

    /// Furnish the floor with items.
    fn furnish(&mut self, rng: &mut GameRng) {
        const MAX_ITEMS: usize = 5;
        let plan = self.floor_plan.clone();
        let level = plan.floor as u8;

        for room in plan.rooms {
            let doors = room.plan.doors.len();
            let (min, max) = (0, MAX_ITEMS.saturating_sub(doors) + 1);
            for _ in 0..rng.gen_range(min..=max) {
                if let Some(coord) = self.choose_item_coord(&room, rng) {
                    if let Ok(&item_kind) = room
                        .room_type
                        .item_kind()
                        .choose_weighted(rng, |item_kind| item_kind.occurrence_prob(level))
                    {
                        let item = Item::new(item_kind);
                        let _ = self.add_item(item, ItemCoord::Lay(coord));
                        // self.place_item(item_key, coord).expect("place item");
                    }
                }
            }
        }
    }

    pub fn can_place_actor(&self, coord: Coord) -> WorldResult<()> {
        self.actors_map
            .can_insert(coord)
            .map_err(WorldError::ActorMap)?;
        if !self.tile_map.is_steppable(coord)? {
            Err(WorldError::NotSteppable(coord))
        } else {
            Ok(())
        }
    }

    fn choose_actor_coord(&self, room: &Room, rng: &mut GameRng) -> Option<Coord> {
        room.plan
            .inside_coords()
            .into_iter()
            .filter(|&coord| self.can_place_actor(coord).is_ok())
            .choose(rng)
    }

    pub fn can_place_item(&self, location: ItemCoord) -> WorldResult<()> {
        self.items_map
            .can_insert(location)
            .map_err(WorldError::ItemMap)?;
        if let ItemCoord::Lay(coord) = location {
            if !self.tile_map.is_steppable(coord)? {
                return Err(WorldError::NotSteppable(coord));
            }
        }
        Ok(())
    }

    fn choose_item_coord(&self, room: &Room, rng: &mut GameRng) -> Option<Coord> {
        room.plan
            .inside_coords()
            .into_iter()
            .filter(|&coord| self.can_place_item(ItemCoord::Lay(coord)).is_ok())
            .choose(rng)
    }

    pub fn plan(&self) -> &FloorPlan {
        &self.floor_plan
    }

    pub fn tile_map(&self) -> &TileMap {
        &self.tile_map
    }

    pub fn time(&self) -> &Time {
        &self.time
    }

    pub fn with_feature(&mut self, coord: Coord, feature: Option<Feature>) -> WorldResult<()> {
        self.tile_map.with_feature(coord, feature)?;

        // Update actors' FOW.
        self.update_sight_maps(coord);

        Ok(())
    }

    pub fn is_steppable(&self, coord: Coord) -> WorldResult<bool> {
        Ok(self.tile_map.is_steppable(coord)? && self.actor_at(coord).is_none())
    }

    /// Move an `Actor` and return its old `Coord`.
    pub fn move_actor(&mut self, actor_key: ActorKey, coord: Coord) -> WorldResult<Coord> {
        self.actor_exist(actor_key)?;
        // self.can_place_actor(coord)?;
        // Move actor to its new coordinates
        let previous_coord = self
            .actors_map
            .r#move(actor_key, coord)
            .map_err(WorldError::ActorMap)?;

        // Update sight map for the actor, with the proper `sight_range` parameter.
        self.update_sight_map(actor_key)?;

        Ok(previous_coord)
    }

    pub fn actor_at(&self, coord: Coord) -> Option<ActorKey> {
        self.actors_map.key(coord)
    }

    pub fn memory_map(&self, actor_key: ActorKey) -> Option<&Array2<bool>> {
        self.memory_map.get(actor_key)
    }

    pub fn steppable_map(&self) -> &Array2<bool> {
        &self.tile_map.steppable_map()
    }

    pub fn impassable_map(&self) -> &Array2<bool> {
        &self.tile_map.impassable_map()
    }

    pub fn sight_map(&self, actor_key: ActorKey) -> Option<&Array2<bool>> {
        self.sight_map.get(actor_key)
    }

    pub fn can_see_tile(&self, actor_key: ActorKey, coord: Coord) -> WorldResult<bool> {
        if let Some(sight_map) = self.sight_map(actor_key) {
            sight_map
                .get(Ix2::from(coord))
                .cloned()
                .ok_or(WorldError::MissingTile(coord))
        } else {
            Ok(false)
        }
    }

    /// Update sight map
    fn update_sight_map(&mut self, actor_key: ActorKey) -> WorldResult<()> {
        let sight_range = self.actor(actor_key)?.sight_range();
        if let Ok(actor_coord) = self.actor_coord(actor_key) {
            let sight_map = actor_coord.l1_fov(&self.tile_map.opacity_map(), sight_range);
            self.sight_map.insert(actor_key, sight_map);
            self.update_memory_map(actor_key);
        } else {
            self.sight_map.remove(actor_key);
        }
        Ok(())
    }

    /// Updates the memory maps of all actors who can see a certain tile.
    /// Useful when modifying a tile changes its transparency value.
    fn update_sight_maps(&mut self, coord: Coord) {
        for (actor_key, actor) in &self.actors {
            if self.can_see_tile(actor_key, coord).expect("sight value") {
                // update sight_map
                if let Ok(actor_coord) = self.actor_coord(actor_key) {
                    let sight_range = actor.sight_range();
                    let sight_map = actor_coord.l1_fov(&self.tile_map.opacity_map(), sight_range);
                    // update memory_map
                    if let Some(memory_map) = self.memory_map.get_mut(actor_key) {
                        Zip::from(memory_map)
                            .and(&sight_map)
                            .for_each(|memory, &sight| *memory |= sight);
                    }
                    self.sight_map.insert(actor_key, sight_map);
                } else {
                    self.sight_map.remove(actor_key);
                }
            }
        }
    }

    fn update_memory_map(&mut self, actor_key: ActorKey) {
        if let Some(memory_map) = self.memory_map.get_mut(actor_key) {
            if let Some(sight_map) = self.sight_map.get(actor_key) {
                Zip::from(memory_map)
                    .and(sight_map)
                    .for_each(|memory, &sight| *memory |= sight);
            }
        }
    }

    pub fn add_memory_map(&mut self, actor_key: ActorKey) {
        self.memory_map
            .insert(actor_key, Array2::from_elem(WORLD_DIM, false));
    }

    pub fn update_fire(&mut self) {
        self.tile_map.update_fire();
    }

    pub fn actors(&self) -> &DenseSlotMap<ActorKey, Actor> {
        &self.actors
    }

    pub fn actors_mut(&mut self) -> &mut DenseSlotMap<ActorKey, Actor> {
        &mut self.actors
    }

    pub fn actor(&self, actor_key: ActorKey) -> WorldResult<&Actor> {
        self.actors
            .get(actor_key)
            .ok_or(WorldError::MissingActor(actor_key))
    }

    pub fn actor_mut(&mut self, actor_key: ActorKey) -> WorldResult<&mut Actor> {
        self.actors
            .get_mut(actor_key)
            .ok_or(WorldError::MissingActor(actor_key))
    }

    // Check if `ActorKey` corresponds to an existing `Actor`.
    pub fn actor_exist(&self, actor_key: ActorKey) -> WorldResult<()> {
        // Check if `the actor_key` corresponds to an existing actor…
        if self.actors.contains_key(actor_key) {
            Ok(())
        } else {
            // …if not, return an error.
            Err(WorldError::MissingActor(actor_key))
        }
    }

    /// Add a new `Actor` into the `World` and return its `ActorKey`.
    ///
    /// * Add the `Actor` to the `World`.
    /// * Add the `Actor` to the time schedule.
    /// * Add the `Actor` to the map.
    /// * If the `Actor` is the player, add a memory map.
    pub fn insert_actor(&mut self, actor: Actor, coord: Coord) -> WorldResult<ActorKey> {
        let alignment = actor.alignment();

        // Add `actor` to set of `actors`.
        let actor_key = self.actors.insert(actor);

        // Add `actor` to time schedule.
        self.time
            .insert(actor_key)
            .expect("actor inserted in schedule");

        if let Alignment::Player = alignment {
            self.add_memory_map(actor_key);
        }

        self.actors_map
            .insert(actor_key, coord)
            .map_err(WorldError::ActorMap)?;

        // Update sight map for the actor, with the proper `sight_range` parameter.
        self.update_sight_map(actor_key)?;

        Ok(actor_key)
    }

    pub fn actor_coord(&self, actor_key: ActorKey) -> WorldResult<Coord> {
        self.actor_exist(actor_key)?;
        let coord = self.actors_map.coord(actor_key).expect("actor's coord");
        Ok(coord)
    }

    /// Remove an actor from the world,
    /// taking care to also remove it from time schedule and from the map.
    /// Returns the removed actor.
    pub fn remove_actor(&mut self, actor_key: ActorKey) -> WorldResult<Actor> {
        // Check the given actor exists.
        self.actor_exist(actor_key)?;

        self.actors_map
            .remove(actor_key)
            .expect("remove actor from map");

        self.time.remove(actor_key).expect(
            "remove actor fr    pub fn item(&self, item_key: ItemKey) -> WorldResult<&Item> {
                self.items
                    .get(item_key)
                    .ok_or(WorldError::MissingItem(item_key))
            }
        
        om schedule",
        );

        // Remove actor from storage
        let actor = self.actors.remove(actor_key).expect("actor to delete");

        Ok(actor)
    }

    /// Move an `Actor` by a step in a given `Dir`.
    /// Returns the actor's previous coordinates.
    pub fn step_actor(&mut self, actor_key: ActorKey, dir: Dir) -> WorldResult<Coord> {
        let coord = self.actor_coord(actor_key)?;
        self.move_actor(actor_key, coord + dir)
    }

    /// Schedule `actor_key` to activate in `waiting_time`.
    pub fn schedule_actor(
        &mut self,
        actor_key: ActorKey,
        waiting_time: TimeUnit,
    ) -> WorldResult<()> {
        // Check if `the actor_key` corresponds to an existing actor.
        self.actor_exist(actor_key)?;

        self.time.schedule_actor(actor_key, waiting_time)?;
        Ok(())
    }

    /// Make world's time tick.
    pub fn time_tick(&mut self) -> WorldResult<()> {
        self.update_fire();
        self.time.tick()?;
        Ok(())
    }

    pub fn item(&self, item_key: ItemKey) -> WorldResult<&Item> {
        self.items
            .get(item_key)
            .ok_or(WorldError::MissingItem(item_key))
    }

    pub fn item_mut(&mut self, item_key: ItemKey) -> WorldResult<&mut Item> {
        self.items
            .get_mut(item_key)
            .ok_or(WorldError::MissingItem(item_key))
    }

    // Check if `ItemKey` corresponds to an existing `Item`.
    pub fn item_exist(&self, item_key: ItemKey) -> WorldResult<()> {
        // Check if the `item_key` corresponds to an existing actor…
        if self.items.contains_key(item_key) {
            Ok(())
        } else {
            // …if not, return an error.
            Err(WorldError::MissingItem(item_key))
        }
    }

    /// Insert a new `Item` into the `World` and return its `ItemKey`.
    pub fn add_item(&mut self, item: Item, location: ItemCoord) -> WorldResult<ItemKey> {
        self.can_place_item(location)?;
        // Add `item` to set of `items`.
        let item_key = self.items.insert(item);
        self.items_map
            .insert(item_key, location)
            .expect("insert item");
        Ok(item_key)
    }

    pub fn move_item(&mut self, item_key: ItemKey, location: ItemCoord) -> WorldResult<ItemCoord> {
        // Check that the given item exists.
        self.item_exist(item_key)?;
        self.can_place_item(location)?;

        let old_location = self
            .items_map
            .r#move(item_key, location)
            .expect("move item");

        Ok(old_location)
    }

    pub fn item_location(&self, item_key: ItemKey) -> WorldResult<ItemCoord> {
        // Check that the given item exists.
        self.item_exist(item_key)?;
        let location = self.items_map.coord(item_key).expect("item location");
        Ok(location)
    }

    pub fn item_coord(&self, item_key: ItemKey) -> WorldResult<Coord> {
        match self.item_location(item_key)? {
            ItemCoord::Fly(_) => Err(WorldError::MissingItem(item_key)),
            ItemCoord::Lay(coord) => Ok(coord),
        }
    }

    pub fn flying_item_coord(&self, item_key: ItemKey) -> WorldResult<Coord> {
        match self.item_location(item_key)? {
            ItemCoord::Lay(_) => Err(WorldError::MissingItem(item_key)),
            ItemCoord::Fly(coord) => Ok(coord),
        }
    }

    pub fn item_at(&self, location: ItemCoord) -> Option<ItemKey> {
        self.items_map.key(location)
    }

    pub fn laying_item_at(&self, coord: Coord) -> Option<ItemKey> {
        self.item_at(ItemCoord::Lay(coord))
    }

    pub fn flying_item_at(&self, coord: Coord) -> Option<ItemKey> {
        self.item_at(ItemCoord::Fly(coord))
    }

    /// Move an `Item` and return its old `Coord`.
    /// Remove an item from the world map.
    pub fn delete_item(&mut self, item_key: ItemKey) -> WorldResult<(Item, ItemCoord)> {
        let item = self
            .items
            .remove(item_key)
            .ok_or(WorldError::MissingItem(item_key))?;

        let location = self.items_map.remove(item_key).expect("remove item");

        Ok((item, location))
    }
}
