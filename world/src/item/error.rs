use std::error::Error;
use std::fmt;

#[derive(Debug)]
pub enum ItemError {
    AnItemError,
}

impl fmt::Display for ItemError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let error_msg = match self {
            ItemError::AnItemError => "an item error",
        };
        write!(f, "{}", error_msg)
    }
}

impl Error for ItemError {}

pub type ItemResult<T> = Result<T, ItemError>;
