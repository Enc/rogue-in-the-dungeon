use crate::actor::{DamageKind, PhysicalDamage};
use serde::{Deserialize, Serialize};

#[derive(Clone, Copy, Debug, Serialize, Deserialize)]
pub enum ItemKind {
    Armor(Armor),
    Potion(Potion),
    Scroll(Scroll),
    Wand(Wand),
    Weapon(Weapon),
}

impl ItemKind {
    pub fn occurrence_prob(self, level: u8) -> u8 {
        const BASE_PROB: u8 = 5;

        match self {
            ItemKind::Weapon(weapon) => match weapon {
                Weapon::Dagger => 2 * BASE_PROB,
                Weapon::Sword => BASE_PROB + level,
                Weapon::WarHammer => 2 * level,
            },
            ItemKind::Armor(armor) => match armor {
                Armor::Leather => 2 * BASE_PROB,
                Armor::Chainmail => BASE_PROB + level,
                Armor::Plate => 2 * level,
            },
            _ => BASE_PROB,
        }
    }
}

#[derive(Clone, Copy, Debug, Serialize, Deserialize)]
pub enum Potion {
    Health,
}

#[derive(Clone, Copy, Debug, Serialize, Deserialize)]
pub enum Scroll {
    AScroll,
}

#[derive(Clone, Copy, Debug, Serialize, Deserialize)]
pub enum Wand {
    Lightning,
    FireBolt,
}

#[derive(Clone, Copy, Debug, Serialize, Deserialize)]
pub enum Weapon {
    Dagger,
    Sword,
    WarHammer,
}

impl Weapon {
    pub fn damage(&self) -> DamageKind {
        match *self {
            Weapon::Dagger => DamageKind::Physical(PhysicalDamage::Piercing),
            Weapon::Sword => DamageKind::Physical(PhysicalDamage::Slashing),
            Weapon::WarHammer => DamageKind::Physical(PhysicalDamage::Bludgeoning),
        }
    }

    pub fn damage_mult(&self) -> usize {
        match *self {
            Weapon::Dagger => 2,
            Weapon::Sword => 3,
            Weapon::WarHammer => 4,
        }
    }
}

#[derive(Clone, Copy, Debug, Serialize, Deserialize)]
pub enum Armor {
    Leather,
    Chainmail,
    Plate,
}

impl Armor {
    pub fn defense(&self) -> u8 {
        match *self {
            Armor::Leather => 2,
            Armor::Chainmail => 4,
            Armor::Plate => 6,
        }
    }

    pub fn dex_malus(&self) -> u8 {
        match *self {
            Armor::Leather => 1,
            Armor::Chainmail => 2,
            Armor::Plate => 3,
        }
    }
}
