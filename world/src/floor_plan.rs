mod corridor;
mod room;

pub use corridor::Corridor;
pub use room::{Doors, Room, RoomPlan, RoomType};

use crate::world::{Coord, CoordIndex, Dir};
use crate::{GameRng, Set, WORLD_COLS, WORLD_ROWS};
use rand::prelude::*;
use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct FloorPlan {
    pub rooms: Vec<Room>,
    pub corridors: Vec<Corridor>,
    pub landing: Coord,
    pub stairs: Coord,
    pub floor: CoordIndex,
}

impl FloorPlan {
    pub fn find_room(&self, coord: Coord) -> Option<&Room> {
        self.rooms.iter().find(|room| room.plan.contains(coord))
    }

    pub fn generate_bsp(floor: CoordIndex, rng: &mut GameRng) -> FloorPlan {
        use RoomType::*;

        const ROOM_TYPES: [RoomType; 4] = [Plain, Barracks, Vaults, Chasm];
        let mut rooms = Vec::new();
        let (mut room_plans, corridors) = Self::bsp(rng);
        room_plans.shuffle(rng);
        let mut stairs = None;
        let mut landing = None;

        for plan in room_plans.into_iter() {
            let room_type = if stairs.is_none() {
                stairs = Some(plan.center());
                RoomType::Stairs
            } else if landing.is_none() {
                landing = Some(plan.center());
                RoomType::Landing
            } else {
                *ROOM_TYPES.choose(rng).expect("choose a room type")
            };
            rooms.push(Room { room_type, plan });
        }

        FloorPlan {
            rooms,
            corridors,
            landing: landing.expect("landing"), // centered_landing,
            stairs: stairs.expect("stairs"),    // stairs.expect("stairs coordinates"),
            floor,
        }
    }

    fn bsp(rng: &mut GameRng) -> (Vec<RoomPlan>, Vec<Corridor>) {
        let starting_room = RoomPlan {
            north: 0,
            south: WORLD_ROWS as CoordIndex - 1,
            west: 0,
            east: WORLD_COLS as CoordIndex - 1,
            doors: Doors::default(),
        };

        let mut room_plans = vec![starting_room];
        let mut final_room_plans = Vec::default();
        let mut corridors = Vec::default();

        // Generate rooms' plans
        while let Some(room_plan) = room_plans.pop() {
            if let Some((a_plan, b_plan, opt_corridor)) = room_plan.split(rng) {
                room_plans.push(a_plan);
                room_plans.push(b_plan);
                if let Some(corridor) = opt_corridor {
                    corridors.push(corridor)
                }
            } else {
                final_room_plans.push(room_plan);
            }
        }

        (final_room_plans, corridors)
    }

    pub fn generate_dungeon(floor: CoordIndex, looseness: u8, rng: &mut GameRng) -> FloorPlan {
        let (mut rooms, landing, stairs) = Self::generate_dungeon_rooms(rng);
        let mut corridors: Vec<Corridor> = Vec::new();
        let mut connected_rooms: Set<Coord> = Set::new();
        let mut direct_connections: Set<(Coord, Coord)> = Set::new();
        let mut last_room = rooms.last().expect("last room of the dungeon");

        for room in &rooms {
            let ul_corner = room.plan.nw_corner();
            if !connected_rooms.contains(&ul_corner) {
                let loose = match looseness {
                    0 => false,
                    1 => rng.gen(),
                    _ => true,
                };

                Self::connect_rooms(
                    &rooms,
                    room,
                    last_room,
                    &mut corridors,
                    &mut connected_rooms,
                    &mut direct_connections,
                    loose,
                );

                last_room = room;
            }
        }

        // THEN add doors for all corridors!
        corridors
            .iter()
            .for_each(|corridor| Self::add_doors(&mut rooms, corridor));

        FloorPlan {
            rooms,
            corridors,
            landing,
            stairs,
            floor,
        }
    }

    fn generate_dungeon_rooms(rng: &mut GameRng) -> (Vec<Room>, Coord, Coord) {
        const ROOM_TYPES: [RoomType; 5] = [
            RoomType::Plain,
            RoomType::Barracks,
            RoomType::Vaults,
            RoomType::Chasm,
            RoomType::Temple,
        ];
        let mut stairs = None;
        let mut landing = None;

        let mut rooms: Vec<Room> = Vec::new();

        // try a suitably large number of times, proportional to the size of the map
        for _ in 0..WORLD_ROWS * WORLD_COLS {
            let north =
                rng.gen_range(0..WORLD_ROWS as CoordIndex - RoomPlan::ROOM_MAX_SIZE) / 2 * 2;
            let west = rng.gen_range(0..WORLD_COLS as CoordIndex - RoomPlan::ROOM_MAX_SIZE) / 2 * 2;
            let size_row = rng.gen_range(RoomPlan::ROOM_MIN_SIZE..=RoomPlan::ROOM_MAX_SIZE) / 2 * 2;
            let size_col = rng.gen_range(RoomPlan::ROOM_MIN_SIZE..=RoomPlan::ROOM_MAX_SIZE) / 2 * 2;
            let plan = RoomPlan {
                north,
                south: north + size_row,
                west,
                east: west + size_col,
                doors: Vec::new(),
            };

            if !rooms.iter().any(|room| plan.intersects(&room.plan)) {
                let room_type = if landing.is_none() {
                    landing = Some(plan.center());
                    RoomType::Landing
                } else if stairs.is_none() {
                    stairs = Some(plan.center());
                    RoomType::Stairs
                } else {
                    *ROOM_TYPES.choose(rng).expect("choose a room type")
                };

                rooms.push(Room { room_type, plan });
            }
        }

        rooms.shuffle(rng);

        (rooms, landing.expect("landing"), stairs.expect("stairs"))
    }

    fn corridors_path(from_room: &Room, to_room: &Room) -> Vec<Corridor> {
        let from_center = Coord {
            row: from_room.plan.center().row / 2 * 2 + 1,
            col: from_room.plan.center().col / 2 * 2 + 1,
        };
        let to_center = Coord {
            row: to_room.plan.center().row / 2 * 2 + 1,
            col: to_room.plan.center().col / 2 * 2 + 1,
        };
        let mut path = Vec::new();

        let start = from_center;
        let length = (to_center.row - from_center.row).abs();

        if to_center.row > from_center.row {
            path.push(Corridor {
                start,
                length,
                dir: Dir::South,
            });
        }

        if to_center.row < from_center.row {
            path.push(Corridor {
                start,
                length,
                dir: Dir::North,
            });
        }

        let start = Coord {
            row: to_center.row,
            col: from_center.col,
        };
        let length = (to_center.col - from_center.col).abs();

        if to_center.col > from_center.col {
            path.push(Corridor {
                start,
                length,
                dir: Dir::East,
            });
        }

        if to_center.col < from_center.col {
            path.push(Corridor {
                start,
                length,
                dir: Dir::West,
            });
        }

        path
    }

    fn connect_rooms(
        rooms: &[Room],
        from_room: &Room,
        to_room: &Room,
        corridors: &mut Vec<Corridor>,
        connected_rooms: &mut Set<Coord>,
        direct_connections: &mut Set<(Coord, Coord)>,
        loose: bool,
    ) {
        let nw_corner = from_room.plan.nw_corner();
        let mut add_to_corridors = Vec::new();
        connected_rooms.insert(nw_corner);

        for corridor in Self::corridors_path(from_room, to_room) {
            let (opt_dug_corridor, opt_room, stop) =
                Self::dig_corridor(rooms, corridors, corridor, loose);
            if let Some(other_room) = opt_room {
                let other_nw_corner = other_room.plan.nw_corner();
                if loose || !connected_rooms.contains(&other_nw_corner) {
                    Self::connect_rooms(
                        rooms,
                        other_room,
                        to_room,
                        corridors,
                        connected_rooms,
                        direct_connections,
                        loose,
                    );
                }
                if direct_connections.insert((nw_corner, other_nw_corner))
                    && direct_connections.insert((other_nw_corner, nw_corner))
                {
                    if let Some(dug_corridor) = opt_dug_corridor {
                        add_to_corridors.push(dug_corridor);
                    }
                } else {
                    break;
                }
            } else if let Some(dug_corridor) = opt_dug_corridor {
                add_to_corridors.push(dug_corridor);
            }
            if stop {
                corridors.extend(add_to_corridors);
                return;
            }
        }
    }

    fn dig_corridor<'a>(
        rooms: &'a [Room],
        corridors: &[Corridor],
        corridor: Corridor,
        loose: bool,
    ) -> (Option<Corridor>, Option<&'a Room>, bool) {
        let mut dug_corridor = corridor;

        while rooms.iter().any(|any_room| {
            any_room.plan.contains(dug_corridor.start)
                && any_room
                    .plan
                    .contains(dug_corridor.start + dug_corridor.dir)
        }) {
            if dug_corridor.length > 1 {
                dug_corridor.start = dug_corridor.start + dug_corridor.dir;
                dug_corridor.length -= 1;
            } else {
                return (None, None, false);
            }
        }

        let mut digger = dug_corridor.start;
        let length_to_dig = dug_corridor.length;

        for length in 1..=length_to_dig {
            if let Some(room) = rooms
                .iter()
                .find(|any_room| any_room.plan.contains(digger + dug_corridor.dir))
            {
                dug_corridor.length = length + 1;
                return (Some(dug_corridor), Some(room), true);
            } else if !loose
                && corridors
                    .iter()
                    .any(|any_corridor| any_corridor.contains(digger + dug_corridor.dir))
            {
                dug_corridor.length = length;
                return (Some(dug_corridor), None, true);
            } else {
                digger = digger + dug_corridor.dir;
            }
        }

        (Some(dug_corridor), None, false)
    }

    fn add_doors(rooms: &mut [Room], corridor: &Corridor) {
        rooms.iter_mut().for_each(|room| {
            if room.plan.contains(corridor.start) {
                room.plan.add_door(corridor.start)
            } else if room.plan.contains(corridor.end()) {
                room.plan.add_door(corridor.end())
            }
        });
    }
}
