//! Provides spatial maps associating keys and coordinates, both ways.

use serde::{Deserialize, Serialize};
use slotmap::{Key, SecondaryMap};
use std::collections::HashMap;
use std::error::Error;
use std::fmt;
use std::fmt::Debug;
use std::hash::Hash;

pub trait Coord: Clone + Copy + Debug + Eq + Hash {}

impl<T: Clone + Copy + Debug + Eq + Hash> Coord for T {}

#[derive(Debug)]
pub enum SptMapError<C: Coord, K: Key> {
    Taken(C, K),
    Placed(C, K),
    UnPlaced(K),
}

impl<C: Coord, K: Key> fmt::Display for SptMapError<C, K> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let error_msg = match self {
            SptMapError::Taken(coord, key) => format!("{:?} is occupied by {:?}", coord, key),
            SptMapError::Placed(coord, key) => format!("{:?} is placed at {:?}", key, coord),
            SptMapError::UnPlaced(key) => format!("{:?} is not placed anywhere", key),
        };
        write!(f, "{}", error_msg)
    }
}

impl<C: Coord, K: Key> Error for SptMapError<C, K> {}

/// A spatial map associating keys (generic over `slotmap::Key`) and coordinates.
/// It allows to retrieve both the coordinates associated to a key (if any)
/// and the key occupying a given coordinate (if any).
/// It does not allow multiple keys in the same location,
/// or to associate multiple locations to the same key.
#[derive(Debug, Serialize, Deserialize)]
pub struct SptMap<C: Coord, K: Key> {
    /// The spatial map of keys.
    keys: HashMap<C, K>,
    /// The coordinates associated to each key.
    coords: SecondaryMap<K, C>,
}

impl<C: Coord, K: Key> SptMap<C, K> {
    /// Returns the coordinates associated to a key, if any.
    pub fn coord(&self, key: K) -> Option<C> {
        self.coords.get(key).cloned()
    }

    /// Returns the key occupying the given coordinates, if any.
    pub fn key(&self, coord: C) -> Option<K> {
        self.keys.get(&coord).cloned()
    }

    /// Is it possoble to insert at a given coordinates?
    /// Returns error if the given coordinates is already occupied.
    pub fn can_insert(&self, coord: C) -> Result<(), SptMapError<C, K>> {
        if let Some(key) = self.keys.get(&coord) {
            Err(SptMapError::Taken(coord, *key))
        } else {
            Ok(())
        }
    }

    /// Inserts the given key at the given coordinates.
    /// Returns an error if the given key was already placed elsewhere,
    /// or if the coordinates are already occupied.
    pub fn insert(&mut self, key: K, coord: C) -> Result<(), SptMapError<C, K>> {
        self.can_insert(coord)?;

        // If `key` is already placed somewhere, return error
        if let Some(current_coord) = self.coords.get(key) {
            return Err(SptMapError::Placed(*current_coord, key));
        }

        // Place key in its new coordinates
        let check = self.keys.insert(coord, key);
        assert!(check.is_none());
        // Give item its new location
        let check = self.coords.insert(key, coord);
        // .expect_none("xxx");
        assert!(check.is_none());
        Ok(())
    }

    /// Removes the given key from its coordinates.
    /// Returns an error if the given key was not placed anywhere.
    pub fn remove(&mut self, key: K) -> Result<C, SptMapError<C, K>> {
        let coord = self.coords.remove(key).ok_or(SptMapError::UnPlaced(key))?;
        let opt_key = self.keys.remove(&coord);
        assert_eq!(Some(key), opt_key);
        Ok(coord)
    }

    /// Moves the given key to the given coordinates.
    /// Returns an error if the given key was not placed anywhere,
    /// or if the coordinates are already occupied.
    pub fn r#move(&mut self, key: K, coord: C) -> Result<C, SptMapError<C, K>> {
        // Check that the target location is free.
        self.can_insert(coord)?;

        // Get the current coord.
        let current_coord = self.coords.get_mut(key).ok_or(SptMapError::UnPlaced(key))?;

        let previous_coord = *current_coord;

        // Give key its new coordinate
        *current_coord = coord;

        let opt_old_key = self.keys.remove(&previous_coord);
        assert_eq!(opt_old_key, Some(key));
        self.keys.insert(coord, key);
        //.expect_none("");

        Ok(previous_coord)
    }
}

impl<C: Coord, K: Key> Default for SptMap<C, K> {
    fn default() -> Self {
        SptMap {
            keys: HashMap::default(),
            coords: SecondaryMap::default(),
        }
    }
}
