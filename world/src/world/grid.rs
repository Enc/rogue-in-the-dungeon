//! Types and methods to handle the world's geometry.

use crate::WORLD_DIM;
use ndarray::{Array2, Dim, Ix, Ix2};
use serde::{Deserialize, Serialize};
use std::collections::VecDeque;
use std::ops::{Add, Sub};

pub type CoordIndex = i32;

/// A Cartesian coordinate.
#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub struct Coord {
    pub col: CoordIndex,
    pub row: CoordIndex,
}

#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub enum ItemCoord {
    Lay(Coord),
    Fly(Coord),
}

/// The four cardinal directions.
#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub enum Dir {
    North,
    South,
    East,
    West,
}

impl Dir {
    fn delta(self) -> (CoordIndex, CoordIndex) {
        match self {
            Dir::North => (-1, 0),
            Dir::South => (1, 0),
            Dir::West => (0, -1),
            Dir::East => (0, 1),
        }
    }
}

/// A convenience array listing the four cardinal directions.
pub const DIRS: [Dir; 4] = [Dir::North, Dir::East, Dir::South, Dir::West];

impl From<Coord> for Ix2 {
    fn from(coord: Coord) -> Self {
        Dim([coord.row as Ix, coord.col as Ix])
    }
}

impl From<(Ix, Ix)> for Coord {
    fn from((ix_row, ix_col): (Ix, Ix)) -> Self {
        Coord {
            row: ix_row as CoordIndex,
            col: ix_col as CoordIndex,
        }
    }
}

impl Coord {
    /// Manhattan distance between coords.
    pub fn distance(from: Coord, to: Coord) -> CoordIndex {
        let delta_row = (to.row - from.row).abs();
        let delta_col = (to.col - from.col).abs();

        delta_row + delta_col
    }

    pub fn ring(&self, radius: u8) -> Vec<Coord> {
        let mut coords = Vec::new();

        let north_boundary = self.row - radius as CoordIndex;
        let south_boundary = self.row + radius as CoordIndex;
        let west_boundary = self.col - radius as CoordIndex;
        let east_boundary = self.col + radius as CoordIndex;

        coords.extend((west_boundary..self.col).map(|col| Coord {
            col,
            row: self.row + west_boundary - col,
        }));

        coords.extend((self.col..east_boundary).map(|col| Coord {
            col,
            row: north_boundary - self.col + col,
        }));

        coords.extend((self.row..south_boundary).map(|row| Coord {
            col: east_boundary + self.row - row,
            row,
        }));

        coords.extend((west_boundary + 1..=self.col).rev().map(|col| Coord {
            col,
            row: south_boundary - self.col + col,
        }));

        coords
    }

    pub fn directions_from_center(&self) -> Vec<Dir> {
        let mut dirs = Vec::new();
        use std::cmp::Ordering;

        match self.row.cmp(&0) {
            Ordering::Greater => dirs.push(Dir::South),
            Ordering::Less => dirs.push(Dir::North),
            Ordering::Equal => {}
        }

        match self.col.cmp(&0) {
            Ordering::Greater => dirs.push(Dir::East),
            Ordering::Less => dirs.push(Dir::West),
            Ordering::Equal => {}
        }

        dirs
    }

    pub fn l1_fov(&self, opacity_map: &Array2<bool>, max_radius: u8) -> Array2<bool> {
        let mut seen_tiles = Array2::from_elem(opacity_map.dim(), false);
        let mut paths = Array2::from_elem(opacity_map.dim(), (0, 0));
        // coord `self` is always covered
        // *covered_tiles
        //     .get_mut(Ix2::from(*self))
        //     .expect("covered tile") = true;
        *paths.get_mut(Ix2::from(*self)).expect("paths") = (1, 0);
        *paths.get_mut(Ix2::from(*self + Dir::North)).expect("paths") = (1, 0);
        *paths.get_mut(Ix2::from(*self + Dir::South)).expect("paths") = (1, 0);
        *paths.get_mut(Ix2::from(*self + Dir::West)).expect("paths") = (1, 0);
        *paths.get_mut(Ix2::from(*self + Dir::East)).expect("paths") = (1, 0);

        for radius in 2..=max_radius {
            let ring = self.ring(radius);
            for coord in ring {
                for dir in (coord - *self).directions_from_center() {
                    if let Some((free_paths, blocked_paths)) =
                        paths.get(Ix2::from(coord - dir)).cloned()
                    {
                        assert_ne!((free_paths, blocked_paths), (0, 0));
                        if let Some((next_free_paths, next_blocked_paths)) =
                            paths.get_mut(Ix2::from(coord))
                        {
                            if *opacity_map
                                .get(Ix2::from(coord - dir))
                                .expect("covered tile1")
                            {
                                *next_blocked_paths += free_paths + blocked_paths;
                            } else {
                                *next_free_paths += free_paths;
                                *next_blocked_paths += blocked_paths;
                            }
                        }
                    }
                }
            }
        }

        for (index, (free_paths, blocked_paths)) in paths.indexed_iter() {
            if 3 * free_paths > 2 * blocked_paths {
                *seen_tiles.get_mut(index).expect("covered tile5") = true;
            }
        }

        Self::l1_expand(opacity_map, &seen_tiles)
    }

    fn l1_expand(opacity_map: &Array2<bool>, seen_tiles: &Array2<bool>) -> Array2<bool> {
        let mut expanded_seen_tiles = seen_tiles.clone();
        for ((row, col), opacity) in opacity_map.indexed_iter() {
            if *opacity {
                *expanded_seen_tiles
                    .get_mut((row, col))
                    .expect("sight value") = *seen_tiles.get((row, col)).expect("sight value")
                    || seen_tiles.get((row + 1, col)).cloned().unwrap_or(false)
                        && !opacity_map.get((row + 1, col)).cloned().unwrap_or(true)
                    || (row > 0
                        && seen_tiles.get((row - 1, col)).cloned().unwrap_or(false)
                        && !opacity_map.get((row - 1, col)).cloned().unwrap_or(true))
                    || seen_tiles.get((row, col + 1)).cloned().unwrap_or(false)
                        && !opacity_map.get((row, col + 1)).cloned().unwrap_or(true)
                    || (col > 0
                        && seen_tiles.get((row, col - 1)).cloned().unwrap_or(false)
                        && !opacity_map.get((row, col - 1)).cloned().unwrap_or(true));
            }
        }
        expanded_seen_tiles
    }

    pub fn dijkstra_map(
        self,
        weigth_map: &Array2<Option<u8>>,
        max_dist: Option<u8>,
    ) -> Array2<Option<u8>> {
        let mut dijkstra_map = Array2::from_elem(WORLD_DIM, None);
        dijkstra_map[Ix2::from(self)] = Some(0);
        let mut walkers: VecDeque<(Coord, u8)> = vec![(self, 0)].into();

        while let Some((walker_coord, walker_dist)) = walkers.pop_front() {
            if max_dist.map(|max| walker_dist < max).unwrap_or(true) {
                for dir in &DIRS {
                    let step_coord = walker_coord + *dir;
                    if let Some(Some(weigth)) = weigth_map.get(Ix2::from(step_coord)) {
                        let dijkstra_value = dijkstra_map
                            .get_mut(Ix2::from(step_coord))
                            .expect("dijkstra map value");
                        if dijkstra_value
                            .map(|dist| walker_dist + weigth < dist)
                            .unwrap_or(true)
                        {
                            *dijkstra_value = Some(walker_dist + weigth);
                            walkers.push_back((step_coord, walker_dist + weigth));
                        }
                    }
                }
            }
        }

        dijkstra_map
    }
}

impl Add<Coord> for Coord {
    type Output = Coord;

    fn add(self, other: Coord) -> Self::Output {
        Coord {
            col: self.col + other.col,
            row: self.row + other.row,
        }
    }
}

impl Sub<Coord> for Coord {
    type Output = Coord;

    fn sub(self, other: Coord) -> Self::Output {
        Coord {
            col: self.col - other.col,
            row: self.row - other.row,
        }
    }
}

impl Add<Dir> for Coord {
    type Output = Coord;

    fn add(self, other: Dir) -> Coord {
        let (d_row, d_col) = other.delta();
        Coord {
            row: self.row + d_row,
            col: self.col + d_col,
        }
    }
}

impl Sub<Dir> for Coord {
    type Output = Coord;

    fn sub(self, other: Dir) -> Coord {
        let (d_row, d_col) = other.delta();
        Coord {
            row: self.row - d_row,
            col: self.col - d_col,
        }
    }
}
