use super::spt_map::SptMapError;
use super::{Coord, ItemCoord};
use crate::actor::ActorError;
use crate::item::ItemError;
use crate::tile::TileError;
use crate::time::TimeError;
use crate::{ActorKey, ItemKey};
use std::error::Error;
use std::fmt;

#[derive(Debug)]
pub enum WorldError {
    Actor {
        actor_key: ActorKey,
        actor_error: ActorError,
    },
    Item {
        item_key: ItemKey,
        item_error: ItemError,
    },
    CannotSplitRoom,
    ImpassableTerrain(Coord),
    MissingActor(ActorKey),
    MissingItem(ItemKey),
    MissingTile(Coord),
    NoMemoryMap(ActorKey),
    NoItem(Coord),
    NotStairs(Coord),
    NotSameFloor,
    OccupiedCoord {
        actor_key: ActorKey,
        coord: Coord,
    },
    Unsteppable(Coord),
    ActorMap(SptMapError<Coord, ActorKey>),
    ItemMap(SptMapError<ItemCoord, ItemKey>),
    Tile {
        coord: Coord,
        tile_error: TileError,
    },
    Time(TimeError),
    Impassable(Coord),
    NoAltar(Coord),
    NoFeature(Coord),
    NoVegetation(Coord),
    NotFlammable(ItemKey),
    NotOnSameTile {
        actor_key: ActorKey,
        item_key: ItemKey,
    },
    NotSteppable(Coord),
    NotEmpty(Coord),
    Uninteracting(Coord),
    UnusableItem(ItemKey),
    UsedAltar(Coord),
}

impl fmt::Display for WorldError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            // Map::Actor { actor_key, .. } => write!(f, "actor {:?} raised error", actor_key),
            // Map::Item { item_key, .. } => write!(f, "item {:?} raised error", item_key),
            // Map::Time(..) => write!(f, "a time scheduler error"),
            _ => write!(f, "a world error"),
        }
    }
}

impl Error for WorldError {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        match self {
            // Self::Actor { actor_error, .. } => Some(actor_error),
            // Self::Item { item_error, .. } => Some(item_error),
            // Self::Time(time_error) => Some(time_error),
            _ => None,
        }
    }
}

pub type WorldResult<T> = Result<T, WorldError>;

impl From<SptMapError<ItemCoord, ItemKey>> for WorldError {
    fn from(item_map_error: SptMapError<ItemCoord, ItemKey>) -> WorldError {
        WorldError::ItemMap(item_map_error)
    }
}

impl From<SptMapError<Coord, ActorKey>> for WorldError {
    fn from(actor_map_error: SptMapError<Coord, ActorKey>) -> WorldError {
        WorldError::ActorMap(actor_map_error)
    }
}

impl From<TimeError> for WorldError {
    fn from(time_error: TimeError) -> WorldError {
        WorldError::Time(time_error)
    }
}
