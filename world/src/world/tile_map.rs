//! Contains the `TileMap` object.

use super::{Coord, WorldError, WorldResult, DIRS};
use crate::floor_plan::{Corridor, FloorPlan, Room, RoomType};
use crate::tile::{Feature, FloorKind, Tile, TileKind, WallKind};
use crate::{GameRng, WORLD_COLS, WORLD_DIM, WORLD_ROWS};
use ndarray::{Array2, Ix2};
use noise::{Fbm, MultiFractal, NoiseFn, Seedable};
use rand::distributions::Bernoulli;
use rand::prelude::*;
use serde::{Deserialize, Serialize};

/// A grid of `Tile`s
#[derive(Serialize, Deserialize)]
pub struct TileMap {
    tiles: Array2<Tile>,
    opacity_map: Array2<bool>,
    steppable_map: Array2<bool>,
    impassable_map: Array2<bool>,
}

impl TileMap {
    pub fn generate(floor_plan: &FloorPlan, rng: &mut GameRng) -> TileMap {
        let filling_tile = Tile::new_wall(WallKind::Stone);
        let mut tile_map = TileMap {
            tiles: Array2::from_elem(WORLD_DIM, filling_tile),
            opacity_map: Array2::from_elem(WORLD_DIM, filling_tile.opaque()),
            steppable_map: Array2::from_elem(WORLD_DIM, filling_tile.steppable()),
            impassable_map: Array2::from_elem(WORLD_DIM, filling_tile.impassable()),
        };

        for corridor in &floor_plan.corridors {
            tile_map.build_corridor(corridor, rng);
        }

        for room in &floor_plan.rooms {
            tile_map.build_room(room, rng);
        }

        tile_map.add_water(rng);
        tile_map.add_vegetation(rng);
        tile_map.update_maps();
        tile_map
    }

    pub fn tile(&self, coord: Coord) -> WorldResult<&Tile> {
        self.tiles
            .get(Ix2::from(coord))
            .ok_or(WorldError::MissingTile(coord))
    }

    pub fn tiles(&self) -> &Array2<Tile> {
        &self.tiles
    }

    pub fn with_feature(&mut self, coord: Coord, feature: Option<Feature>) -> WorldResult<()> {
        let tile = self
            .tiles
            .get_mut(Ix2::from(coord))
            .ok_or(WorldError::MissingTile(coord))?;

        tile.with_feature(feature);

        // update opacity map
        *self
            .opacity_map
            .get_mut(Ix2::from(coord))
            .expect("transparency value") = self.tile(coord).expect("tile").opaque();

        // update steppable map
        *self
            .steppable_map
            .get_mut(Ix2::from(coord))
            .expect("steppable value") = self.tile(coord).expect("tile").steppable();

        // update impassable map
        *self
            .impassable_map
            .get_mut(Ix2::from(coord))
            .expect("steppable value") = self.tile(coord).expect("tile").impassable();

        Ok(())
    }

    fn update_maps(&mut self) {
        for ((row, col), tile) in self.tiles.indexed_iter() {
            self.steppable_map[(row, col)] = tile.steppable();
            self.opacity_map[(row, col)] = tile.opaque();
            self.impassable_map[(row, col)] = tile.impassable();
        }
    }

    pub fn opacity_map(&self) -> &Array2<bool> {
        &self.opacity_map
    }

    pub fn is_opaque(&self, coord: Coord) -> WorldResult<bool> {
        self.opacity_map
            .get(Ix2::from(coord))
            .cloned()
            .ok_or(WorldError::MissingTile(coord))
    }

    pub fn steppable_map(&self) -> &Array2<bool> {
        &self.steppable_map
    }

    pub fn is_steppable(&self, coord: Coord) -> WorldResult<bool> {
        self.steppable_map
            .get(Ix2::from(coord))
            .cloned()
            .ok_or(WorldError::MissingTile(coord))
    }

    pub fn impassable_map(&self) -> &Array2<bool> {
        &self.impassable_map
    }

    pub fn is_impassable(&self, coord: Coord) -> WorldResult<bool> {
        self.impassable_map
            .get(Ix2::from(coord))
            .cloned()
            .ok_or(WorldError::MissingTile(coord))
    }

    pub fn build_corridor(&mut self, corridor: &Corridor, _rng: &mut GameRng) {
        let mut digger = corridor.start;
        for _ in 0..corridor.length {
            self.tiles[Ix2::from(digger)] = Tile::new_floor(FloorKind::Soil);
            digger = digger + corridor.dir;
        }
    }

    pub fn build_room(&mut self, room: &Room, _rng: &mut GameRng) {
        let plan = &room.plan;

        let wall_type = room.room_type.wall();
        let floor_type = room.room_type.floor();

        for coord in plan.floor_coords() {
            self.tiles[Ix2::from(coord)] = Tile::new_floor(floor_type);
        }
        for coord in plan.wall_coords() {
            self.tiles[Ix2::from(coord)] = Tile::new_wall(wall_type);
        }

        match room.room_type {
            RoomType::Stairs => {
                self.tiles[Ix2::from(plan.center())] = Tile::new(TileKind::Stairs);
            }
            RoomType::Chasm => {
                for coord in plan.inner_coords() {
                    self.tiles[Ix2::from(coord)] = Tile::new(TileKind::Chasm);
                }
            }
            RoomType::Vaults => {
                for row in plan.north / 2 + 1..plan.south / 2 {
                    for col in plan.west / 2 + 1..plan.east / 2 {
                        let coord = Coord {
                            row: 2 * row,
                            col: 2 * col,
                        };
                        self.tiles[Ix2::from(coord)] = Tile::new_wall(wall_type);
                    }
                }
            }
            RoomType::Temple => {
                self.with_feature(plan.center(), Some(Feature::Altar { used: false }))
                    .expect("set feature");
            }
            _ => {}
        }

        for (door_side, index) in &plan.doors {
            let coord = plan.door_coord(*door_side, *index);
            self.tiles[Ix2::from(coord)] = Tile::new_floor(floor_type);
            self.with_feature(coord, Some(Feature::Door { open: false }))
                .expect("set feature");
        }
    }

    /// Add vegetation using cellular automata.
    fn add_vegetation(&mut self, rng: &mut GameRng) {
        const CELLULAR_ITER: u8 = 12;
        const CELLULAR_PROB: f64 = 0.5;
        let distribution = Bernoulli::new(CELLULAR_PROB).unwrap();

        for row in 0..WORLD_ROWS {
            for col in 0..WORLD_COLS {
                let coord = Coord::from((row, col));
                let tile = self.tile(coord).expect("tile");
                if tile.kind().is_fertile() && tile.feature().is_none() && rng.sample(distribution)
                {
                    self.with_feature(coord, Some(Feature::Vegetation { trampled: false }))
                        .expect("set feature");
                }
            }
        }

        for _ in 0..CELLULAR_ITER {
            for row in 0..WORLD_ROWS {
                for col in 0..WORLD_COLS {
                    let coord = Coord::from((row, col));
                    self.vegetation_generation(coord, rng);
                }
            }
        }
    }

    /// Next cellular automata generation for vegetation.
    fn vegetation_generation(&mut self, coord: Coord, rng: &mut GameRng) {
        let tile = self.tiles[Ix2::from(coord)];
        if tile.kind().is_fertile() {
            let mut neighbors = 0;
            if let None | Some(Feature::Vegetation { .. }) = tile.feature() {
                for &dir in &DIRS {
                    if let Ok(tile) = self.tile(coord + dir) {
                        if let Some(Feature::Flooded) | Some(Feature::Vegetation { .. }) =
                            tile.feature()
                        {
                            neighbors += 1;
                        }
                    }
                }
                match neighbors {
                    0 | 1 => self.with_feature(coord, None).expect("set feature"),
                    3 => self
                        .with_feature(coord, Some(Feature::Vegetation { trampled: false }))
                        .expect("set feature"),
                    4 if rng.gen() => self.with_feature(coord, None).expect("set feature"),
                    _ => {}
                }
            }
        }
    }

    /// Add water to level using Perlin noise.
    fn add_water(&mut self, rng: &mut GameRng) {
        let mut noise = Fbm::new();
        noise = noise.set_seed(rng.gen());
        // noise.set_octaves(self, octaves: usize);
        noise = noise.set_frequency(0.05);
        // noise.set_lacunarity(self, lacunarity: f64);
        // noise.set_persistence(self, persistence: f64);
        for ((row, col), tile) in self.tiles.indexed_iter_mut() {
            let heigth = noise.get([row as f64, col as f64]);
            if heigth < -0.15f64 && tile.feature().is_none() {
                match tile.kind() {
                    TileKind::Wall(_) => *tile = Tile::new_wall(WallKind::Molded),
                    TileKind::Floor { .. } => tile.with_feature(Some(Feature::Flooded)),
                    _ => continue,
                }
            }
        }
    }

    pub fn update_fire(&mut self) {
        for tile in self.tiles.iter_mut() {
            if let Some(Feature::Fire { fuel }) = tile.feature() {
                if fuel > 0 {
                    tile.with_feature(Some(Feature::Fire { fuel: fuel - 1 }));
                } else {
                    tile.with_feature(None);
                }
            }
        }
    }
}
