mod error;
mod kind;

pub use error::{ItemError, ItemResult};
pub use kind::{Armor, ItemKind, Potion, Scroll, Wand, Weapon};

use serde::{Deserialize, Serialize};

#[derive(Clone, Copy, Debug, Serialize, Deserialize)]
pub struct Item {
    kind: ItemKind,
}

impl Item {
    pub fn new(kind: ItemKind) -> Item {
        Item { kind }
    }

    pub fn kind(&self) -> ItemKind {
        self.kind
    }
}
