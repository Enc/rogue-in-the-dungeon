# Rogue in the Dungeon - Design Document

## Pitch

Endless dungeon. The goal is to score the highest before dying.

### Ideas

Orkish Cave: instead of a dungeon, it's an orkish settlement.
It would be more structured and varied than a plain dungeon.

### Questions

- What about elves and dwarves?

## Mobs

- Bat: flies and drains hp by sucking blood.
- Dragon: elemental breath attack.
- Goblin: well-equipped; ranged attacks.
- Kobold: well-equipped; magical attacks.
- Ork: well-equipped; melee attacks.
- Rat: weak but common pest. Poisonous bite.
- Skeleton: immune to slashing/bleeding damage.
- Troll: resistant/immune to magical attacks.
- Wraith: immune to phisical attacks; can cross walls.

## Damage

- Piercing: ignores armor.
- Slashing: causes bleeding.
- Bludgeoning: stuns? pushback?
- Elemental

## Elements

- Fire: burns vegetation, doors, and scrolls; extinguished by water.
- Cold: freezes water and mobs over it; explodes potions.
- Electricity: extend effect in water.

## Status

- Bleeding: continued, delayed damage over time.
- Poisoned: continued, delayed damage over time (same as bleeding?).
- Stunned: skip turn?
- Blinded: cannot see in the distance.
- Frozen: cannot move.
- Invisibile: cannot be seen.

## Items

### Weapons

- Dagger: melee piercing damage.
- Sword: melee slashing damage.
- Mace: melee bludgeoning damage.
- Box: ranged piercing damage.

### Armors

### Potions

Potions can be drunk. Their effect applies to the user only.

- Health: restore health.
- Dexterity: temporarily augments DEX.
- Strength: temporarily augments STR.
- Intelligence: temporarily augments INT.
- Perception: temporarily augments PER.
- Ghosting: temporarily turns the user into a ghost, making it immune to physical attacks and able to pass through walls.

### Scrolls

Scrolls can be read. They have area effects.

### Wands

Wands can be zapped in a direction.

- Fireball: zaps a fireball that sets on fire inflammables along its path and explodes on impact.
- Ice: zaps a freezing dart that freezes mobs and water on impact.
- Lightning: zaps an electric lightning that passes through mobs.

## Rooms

- Alchemy lab: inhabited by kobold mages. Contain potions.
- Arena: inhabited by a random pair of monsters.
- Barraks: inhabited by orkish and goblin soldiers. Contain weapons and armors.
- Cave: ???. Inhabited by bats.
- Chasm: has large chasm in the middle. Inhabited by bats.
- Crypt: tombs can be looted but may spawn wraiths and skeletons.
- Dragon lair: inhabited by dragons. Contains treasure.
- Library: inhabited by kobold mages. Contain scrolls.
- Smithery: inhabited by trolls. Contain forges to repair weapons/armors.
- Temple: contain altars.
- Warehouse: inhabited by rats. Contains food.
