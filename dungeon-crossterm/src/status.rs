use crate::dictionary::*;
use game::{Action, ActorError, Feature, FloorKind, GameError, TileKind, WorldError, WorldView};

pub fn status(world_view: &WorldView) -> String {
    let player_key = world_view.pov_key();
    let player_coord = world_view.actor_coord(player_key).expect("player's coord");
    let tile = world_view
        .see_tile(player_coord)
        .expect("tile")
        .expect("see tile");
    if let Ok(Some(item_key)) = world_view.item_at(player_coord) {
        let item = world_view
            .item(item_key)
            .expect("an item")
            .expect("see item");
        format!("There is a {} at your feet.", item_name(item))
    } else if let Some(feature) = tile.feature() {
        String::from(match feature {
            Feature::Blood { .. } => "You are standing in a puddle of blood.",
            Feature::Door { open } if open => "You are standing under a door.",
            Feature::Flooded => "You are knee-deep in slimy stagnant water.",
            Feature::Vegetation { .. } => "You are standing on a patch of grass.",
            Feature::Fire { .. } => "You are enveloped by flames!",
            _ => "You are standing on something???",
        })
    } else {
        String::from(match tile.kind() {
            TileKind::Floor(floor_kind) => match floor_kind {
                FloorKind::Plank => "You are standing on wooden planks.",
                FloorKind::Soil => "You are standing on bare ground.",
                FloorKind::Stone => "You are standing on a stone floor.",
            },
            TileKind::Stairs => "You are standing on a staircase leading downward.",
            _ => "You are standing on something???",
        })
    }
    // } else {
    //     String::from("YOU ARE DEAD!!!")
    // }
}

pub fn action_outcome(world_view: &WorldView, action: Action, outcome: GameError) -> String {
    match action {
        Action::Step { .. } => {
            if let GameError::World(WorldError::NotSteppable(coord)) = outcome {
                let tile = world_view.see_tile(coord).expect("tile").expect("see tile");
                if !tile.kind().steppable() {
                    return format!("You cannot step into a {}.", tilekind_name(tile.kind()));
                } else if let Some(feature) = tile.feature() {
                    if !feature.steppable() {
                        return format!("You cannot step into a {}.", feature_name(feature));
                    }
                }
            }

            format!("{:?} fails because {:?}", action, outcome)
        }
        Action::Pick => match outcome {
            GameError::World(WorldError::NoItem(..)) => {
                String::from("There is nothing to pick up here.")
            }
            GameError::World(WorldError::Actor {
                actor_error: ActorError::InventoryFull,
                ..
            }) => String::from("You are fully loaded and cannot carry any further item."),
            _ => format!("{:?} fails because {:?}", action, outcome),
        },
        _ => format!("{:?} fails because {:?}", action, outcome),
    }
}
