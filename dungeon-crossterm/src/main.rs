//! # Rogue in the Dungeon
//!
//! Rogue in the Dungeon is a traditional fantasy roguelike game.
//!
//! The player delves into a dungeon, fighting the creatures and monsters that inhabit it.
//! The goal is simply to descend as deep as possible before dying.
//!
//! This documentation is a technical reference for the internals of the game engine.

#![forbid(unsafe_code)]

use crossterm::{
    cursor::{Hide, MoveTo, RestorePosition, SavePosition, Show},
    execute, queue,
    style::{Color, Print, SetBackgroundColor, SetForegroundColor},
    terminal::{
        disable_raw_mode, enable_raw_mode, Clear, ClearType, EnterAlternateScreen,
        LeaveAlternateScreen, SetTitle,
    },
};
use std::io::{stdout, Write};

use dungeon_crossterm::View;
use game::{Game, GameSeed};

fn main() {
    println!("Game starting");

    execute!(
        stdout(),
        EnterAlternateScreen,
        SetTitle("Rogue in the Dungeon"),
        SavePosition,
        Hide,
    )
    .expect("initialize screen");

    enable_raw_mode().expect("enable raw mode");

    main_menu();

    disable_raw_mode().expect("disable raw mode");

    execute!(stdout(), LeaveAlternateScreen, Show, RestorePosition).expect("restore screen");

    println!("Game ended");
}

const TITLE: &str = "
▀███▀▀▀██▄                                       ██               ██  ███                 ▀███▀▀▀██▄                                                           
  ██   ▀██                                                        ██   ██                   ██    ▀██▄                                                         
  ██   ▄██  ▄██▀██▄ ▄█▀███████  ▀███   ▄█▀██   ▀███ ▀████████▄  ██████ ███████▄   ▄█▀██     ██     ▀█████  ▀███ ▀████████▄  ▄█▀█████ ▄█▀██  ▄██▀██▄▀████████▄  
  ███████  ██▀   ▀████  ██  ██    ██ ▄█▀   ██    ██   ██    ██    ██   ██    ██ ▄█▀   ██    ██      ██ ██    ██   ██    ██ ▄██  ██ ▄█▀   ████▀   ▀██ ██    ██  
  ██  ██▄  ██     ███████▀  ██    ██ ██▀▀▀▀▀▀    ██   ██    ██    ██   ██    ██ ██▀▀▀▀▀▀    ██     ▄██ ██    ██   ██    ██ ▀█████▀ ██▀▀▀▀▀▀██     ██ ██    ██  
  ██   ▀██▄██▄   ▄███       ██    ██ ██▄    ▄    ██   ██    ██    ██   ██    ██ ██▄    ▄    ██    ▄██▀ ██    ██   ██    ██ ██      ██▄    ▄██▄   ▄██ ██    ██  
▄████▄ ▄███▄▀█████▀ ███████ ▀████▀███▄▀█████▀  ▄████▄████  ████▄  ▀███████  ████▄▀█████▀  ▄████████▀   ▀████▀███▄████  ████▄███████ ▀█████▀ ▀█████▀▄████  ████▄
                  █▀     ██                                                                                               █▀     ██                           
                  ██████▀                                                                                                 ██████▀                             
";
const GAME_INTRO: &str = "Countless adventurers and rogues have entered the Bottomless Dungeon,
lured by the riches beyond measure which it is said to contain.
Such fools!
Crawling with ferocious fiends and ripe with danger,
the Dungeon is a fatal trap from which no one ever returned.";

const MENU: &str = "Press [ENTER] to start/resume a game, [ESC] to quit.";

fn print_main_menu() {
    queue!(
        stdout(),
        Clear(ClearType::All),
        SetForegroundColor(Color::White),
        SetBackgroundColor(Color::Black),
    )
    .expect("clear screen");

    for (line, title) in TITLE.lines().enumerate() {
        queue!(stdout(), MoveTo(0, line as u16), Print(title),).expect("print title");
    }

    for (line, intro) in GAME_INTRO.lines().enumerate() {
        queue!(stdout(), MoveTo(0, 13 + line as u16), Print(intro),).expect("print intro");
    }

    queue!(stdout(), MoveTo(0, 21), Print(MENU),).expect("print menu");

    stdout().flush().expect("flush to output stream");
}

fn main_menu() {
    use crossterm::event::{read, Event, KeyCode};

    loop {
        print_main_menu();

        match read().expect("event") {
            Event::Key(key_event) => match key_event.code {
                KeyCode::Esc => return,
                KeyCode::Enter => {
                    execute!(stdout(), Clear(ClearType::All)).expect("clear screen");
                    run_game();
                }
                _ => continue,
            },
            _ => continue,
        }
    }
}

fn run_game() {
    use rand::prelude::*;

    let mut game = Game::load_game().unwrap_or(Game::generate(random(), Box::new(View::default())));
    game.run().expect("terminate game without errors");
}
