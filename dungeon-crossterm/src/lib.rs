mod dictionary;
mod log;
mod status;

use crossterm::{
    cursor::MoveTo,
    execute, queue,
    style::{Color, Print, SetBackgroundColor, SetForegroundColor},
    terminal::{Clear, ClearType},
};
use dictionary::*;
use game::Stat;
use game::WorldView;
use game::{AIError, AIResult, GameResult, Player};
use game::{Action, Coord, Dir, Event, GameError, ItemKey, WORLD_COLS, WORLD_ROWS};
use game::{Feature, TileKind};
use serde::{Deserialize, Serialize};
use std::io::{stdout, Write};

#[derive(Debug, Default, Deserialize, Serialize)]
pub struct View {
    log: Vec<String>,
}

#[typetag::serde]
impl Player for View {
    fn decide(&mut self, world_view: &WorldView) -> AIResult<Action> {
        use crossterm::event::{read, Event, KeyCode};

        self.frame(world_view);

        loop {
            match read().expect("event") {
                Event::Key(key_event) => {
                    return match key_event.code {
                        KeyCode::Up => match self.dir_action(world_view, Dir::North) {
                            Some(action) => Ok(action),
                            None => continue,
                        },
                        KeyCode::Down => match self.dir_action(world_view, Dir::South) {
                            Some(action) => Ok(action),
                            None => continue,
                        },
                        KeyCode::Left => match self.dir_action(world_view, Dir::West) {
                            Some(action) => Ok(action),
                            None => continue,
                        },
                        KeyCode::Right => match self.dir_action(world_view, Dir::East) {
                            Some(action) => Ok(action),
                            None => continue,
                        },
                        KeyCode::Char(num) if num > '0' && num < '9' => {
                            let num = num as usize - '0' as usize - 1;
                            // let inventory = world_view.actor_pov().items();
                            if let Some((item_key, _)) =
                                world_view.pov_actor().items().iter().nth(num)
                            {
                                if let Some(action) = self.item_query(world_view, item_key) {
                                    Ok(action)
                                } else {
                                    self.status(world_view);
                                    continue;
                                }
                            } else {
                                self.status(world_view);
                                continue;
                            }
                        }
                        KeyCode::Char('.') => Ok(Action::Wait),
                        KeyCode::Char('g') => Ok(Action::Pick),
                        KeyCode::Char('>') => Ok(Action::DescendStairs),
                        KeyCode::Esc => Err(AIError::Quit),
                        _ => continue,
                    };
                }
                Event::Resize(_, _) => {
                    // TODO: check if screen too small.
                    queue!(stdout(), Clear(ClearType::All)).expect("clear screen");
                    self.frame(world_view);
                    continue;
                }
                _ => continue,
            }
        }
    }

    fn react(&mut self, world_view: &WorldView, event: &Event) -> AIResult<()> {
        use game::{ActorEvent, ItemEvent, TileEvent};
        const FRAME_TIME: std::time::Duration = std::time::Duration::from_millis(250);
        // self.view.render(world_view);
        self.log(world_view, event);
        match *event {
            Event::Item(_, item_event) => match item_event {
                ItemEvent::Fly => {
                    self.frame(world_view);
                    std::thread::sleep(FRAME_TIME);
                }
                _ => {}
            },
            Event::Tile(coord, tile_event) => match tile_event {
                TileEvent::Lightning => {
                    self.lightning(coord);
                    std::thread::sleep(FRAME_TIME);
                }
                TileEvent::FireBolt => {
                    self.firebolt(coord);
                    std::thread::sleep(FRAME_TIME);
                }
                _ => {}
            },
            Event::Actor(actor_key, ref actor_event) => match *actor_event {
                ActorEvent::Die => {
                    if world_view.pov_key() == actor_key {
                        self.frame(world_view);
                        std::thread::sleep(FRAME_TIME);
                        return Err(AIError::Quit);
                    }
                }
                _ => {}
            },
            _ => {}
        }
        Ok(())
    }

    fn outcome(&mut self, world_view: &WorldView, action: Action, outcome: GameResult<()>) {
        if let Err(game_error) = outcome {
            self.action_outcome(world_view, action, game_error);
        } else {
            self.status(world_view);
        }
        // Do something?
    }
}

impl View {
    const PADDING_TOP: u16 = 1;
    const PADDING_LEFT: u16 = 20;

    fn dir_action(&self, world_view: &WorldView, dir: Dir) -> Option<Action> {
        let actor_key = world_view.pov_key();
        let player_coord = world_view.actor_coord(actor_key).expect("player's coord");
        let coord = player_coord + dir;
        if world_view
            .actor_at(coord)
            .expect("look for actors")
            .is_some()
        {
            Some(Action::MeleeAttack(dir))
        } else {
            let tile = world_view.see_tile(coord).expect("tile").expect("see tile");
            match tile.feature() {
                Some(Feature::Door { open }) if !open => Some(Action::OpenDoor(dir)),
                Some(Feature::Altar { .. }) => match self.stat_query() {
                    Some(stat) => Some(Action::PrayAltar { dir, stat }),
                    _ => {
                        self.status(world_view);
                        None
                    }
                },
                _ => match tile.kind() {
                    TileKind::Chasm => match self.confirm_query(&crate::dictionary::JUMP_CONFIRM) {
                        Some(true) => Some(Action::Jump(dir)),
                        _ => {
                            self.status(world_view);
                            None
                        }
                    },
                    _ => Some(Action::Step(dir)),
                },
            }
        }
    }

    fn frame(&self, world_view: &WorldView) {
        // Clearing the screen causes flickering
        // queue!(
        //     stdout(),
        //     Clear(ClearType::All)
        // );
        self.print_log();
        self.print_life(world_view);
        self.print_inventory(world_view);
        self.draw_world(world_view);
        stdout().flush().expect("flush to output stream");
    }

    fn draw_world(&self, view: &WorldView) {
        let mut stdout = stdout();
        for row in 0..WORLD_ROWS as i32 {
            for col in 0..WORLD_COLS as i32 {
                let coord = Coord { col, row };

                let (background_color, foreground_color, symbol) = self.draw_cell(view, coord);

                queue!(
                    stdout,
                    MoveTo(
                        col as u16 + Self::PADDING_LEFT,
                        row as u16 + Self::PADDING_TOP
                    ),
                    SetForegroundColor(foreground_color),
                    SetBackgroundColor(background_color),
                    Print(symbol),
                )
                .expect("print cell");
            }
        }
    }

    fn draw_cell(&self, view: &WorldView, coord: Coord) -> (Color, Color, char) {
        let foreground;
        let background;
        let symbol;

        if let Some(tile) = view.see_tile(coord).expect("seen tile") {
            if let Some(actor_key) = view.actor_at(coord).expect("get actor") {
                let actor = view.actor(actor_key).expect("actor").expect("actor");
                symbol = actor_symb(actor);
            } else if let Some(item_key) = view.flying_item_at(coord).expect("get items") {
                let item = view.item(item_key).expect("item").expect("item");
                symbol = item_symb(item);
            } else if let Some(item_key) = view.item_at(coord).expect("get items") {
                let item = view.item(item_key).expect("item").expect("item");
                symbol = item_symb(item);
            } else {
                symbol = tile_symb(*tile);
            }
            background = tile_background_color(tile);
            foreground = Color::Black;
        } else if let Ok(Some(tile_kind)) = view.remember_tile(coord) {
            background = Color::Black;
            foreground = tilekind_background_color(tile_kind);
            symbol = tilekind_symb(tile_kind);
        } else {
            background = Color::Black;
            foreground = Color::Black;
            symbol = ' ';
        }
        (background, foreground, symbol)
    }

    fn print_status(&self, status: &str) {
        execute!(
            stdout(),
            MoveTo(0, 0),
            SetForegroundColor(Color::White),
            SetBackgroundColor(Color::Black),
            Clear(ClearType::CurrentLine),
            Print(status),
        )
        .expect("print status");
    }

    pub fn status(&self, world_view: &WorldView) {
        use crate::status::*;
        let status = status(world_view);
        self.print_status(&status);
    }

    pub fn action_outcome(&self, world_view: &WorldView, action: Action, outcome: GameError) {
        use crate::status::*;
        let action_outcome = action_outcome(world_view, action, outcome);
        self.print_status(&action_outcome);
    }

    pub fn log(&mut self, world_view: &WorldView, event: &Event) {
        use crate::log::*;
        if let Some(entry) = log_event(world_view, event) {
            if log_new_line(event) {
                self.log.push(entry);
            } else if let Some(previous_entry) = self.log.last_mut() {
                *previous_entry += &format!(" {}", entry);
            }
        }
    }

    pub fn print_log(&self) {
        const NUM_LINES: usize = 3;
        let mut stdout = stdout();

        queue!(
            stdout,
            SetForegroundColor(Color::White),
            SetBackgroundColor(Color::Black),
        )
        .expect("set log style");

        for (line, entry) in self.log.iter().rev().take(NUM_LINES).rev().enumerate() {
            queue!(
                stdout,
                MoveTo(0, line as u16 + Self::PADDING_TOP + WORLD_ROWS as u16),
                Clear(ClearType::CurrentLine),
                Print(entry),
            )
            .expect("print log entry");
        }
    }

    fn print_inventory(&self, world_view: &WorldView) {
        let mut stdout = stdout();

        queue!(
            stdout,
            SetForegroundColor(Color::White),
            SetBackgroundColor(Color::Black),
            MoveTo(
                Self::PADDING_LEFT + WORLD_COLS as u16 + 1,
                Self::PADDING_TOP + 1
            ),
            Print("Inventory"),
            MoveTo(
                Self::PADDING_LEFT + WORLD_COLS as u16 + 1,
                Self::PADDING_TOP + 2
            ),
            Print("---------"),
        )
        .expect("print inventory header");

        for line in 0..10 {
            queue!(
                stdout,
                MoveTo(
                    Self::PADDING_LEFT + WORLD_COLS as u16 + 1,
                    Self::PADDING_TOP + 3 + line
                ),
                Clear(ClearType::UntilNewLine),
            )
            .expect("clear inventory");
        }

        for (idx, (_, item)) in world_view.pov_actor().items().iter().enumerate() {
            queue!(
                stdout,
                MoveTo(
                    Self::PADDING_LEFT + WORLD_COLS as u16 + 1,
                    Self::PADDING_TOP + 3 + idx as u16
                ),
                Print(format!("{}) {}", idx + 1, item_name(item))),
            )
            .expect("print inventory item");
        }
    }

    fn item_query(&self, world_view: &WorldView, item_key: ItemKey) -> Option<Action> {
        use crossterm::event::{read, Event, KeyCode};

        let item = world_view.pov_actor().item(item_key).expect("see item");
        let query = format!(
            "{}: (t)hrow, (d)rop, (q)aff, (w)ear, (W)ield or (z)ap?",
            item_name(item)
        );
        self.print_status(&query);

        loop {
            match read().expect("event") {
                Event::Key(key_event) => {
                    return match key_event.code {
                        KeyCode::Char(chr) => match chr {
                            'd' => Some(Action::DropItem(item_key)),
                            'q' => Some(Action::Quaff(item_key)),
                            'w' => Some(Action::EquipArmor(item_key)),
                            'W' => Some(Action::EquipWeapon(item_key)),
                            't' => {
                                if let Some(dir) = self.direction_query() {
                                    Some(Action::Throw { item_key, dir })
                                } else {
                                    None
                                }
                            }
                            'z' => {
                                if let Some(dir) = self.direction_query() {
                                    Some(Action::Zap {
                                        wand_key: item_key,
                                        dir,
                                    })
                                } else {
                                    None
                                }
                            }
                            _ => None,
                        },
                        KeyCode::Esc => None,
                        _ => continue,
                    };
                }
                _ => continue,
            }
        }
    }

    fn confirm_query(&self, message: &str) -> Option<bool> {
        use crossterm::event::{read, Event, KeyCode};

        self.print_status(&format!("{} (y/n)", message));

        loop {
            match read().expect("event") {
                Event::Key(key_event) => {
                    return match key_event.code {
                        KeyCode::Char(chr) => match chr {
                            'y' | 'Y' => Some(true),
                            'n' | 'N' => Some(false),
                            _ => None,
                        },
                        KeyCode::Esc => None,
                        _ => continue,
                    };
                }
                _ => continue,
            }
        }
    }

    fn direction_query(&self) -> Option<Dir> {
        use crossterm::event::{read, Event, KeyCode};

        self.print_status("In which direction? (n/s/w/e)");

        loop {
            match read().expect("event") {
                Event::Key(key_event) => {
                    return match key_event.code {
                        KeyCode::Char(chr) => match chr {
                            'n' | 'N' => Some(Dir::North),
                            's' | 'S' => Some(Dir::South),
                            'e' | 'E' => Some(Dir::East),
                            'w' | 'W' => Some(Dir::West),
                            _ => None,
                        },
                        KeyCode::Up => Some(Dir::North),
                        KeyCode::Down => Some(Dir::South),
                        KeyCode::Right => Some(Dir::East),
                        KeyCode::Left => Some(Dir::West),
                        KeyCode::Esc => None,
                        _ => continue,
                    };
                }
                _ => continue,
            }
        }
    }

    fn stat_query(&self) -> Option<Stat> {
        use crossterm::event::{read, Event, KeyCode};

        self.print_status("Which stat do you want to increase? (c)onstitution; (s)trength; (d)exterity; (p)erception; (i)ntelligence");

        loop {
            match read().expect("event") {
                Event::Key(key_event) => {
                    return match key_event.code {
                        KeyCode::Char(chr) => match chr {
                            'c' | 'C' => Some(Stat::Con),
                            's' | 'S' => Some(Stat::Str),
                            'd' | 'D' => Some(Stat::Dex),
                            'p' | 'P' => Some(Stat::Per),
                            'i' | 'I' => Some(Stat::Int),
                            _ => None,
                        },
                        KeyCode::Esc => None,
                        _ => continue,
                    };
                }
                _ => continue,
            }
        }
    }

    fn print_life(&self, world_view: &WorldView) {
        let actor = world_view.pov_actor();
        self.print_bar(
            "HP",
            (actor.health_points(), actor.max_health_points()),
            Color::Red,
            Color::DarkRed,
            (0, Self::PADDING_TOP + 1),
            Self::PADDING_LEFT as u8 - 2,
        );
    }

    fn print_bar(
        &self,
        label: &str,
        value: (u8, u8),
        fore_color: Color,
        back_color: Color,
        pos: (u16, u16),
        length: u8,
    ) {
        let mut stdout = stdout();

        let fore_length = length as usize * value.0 as usize / value.1 as usize;
        let label_value = format!(" {}: {}/{}", label, value.0, value.1);

        queue!(
            stdout,
            MoveTo(pos.0, pos.1),
            SetForegroundColor(Color::Black),
            SetBackgroundColor(fore_color),
        )
        .expect("set bar style");

        for (i, chr) in label_value
            .chars()
            .chain(" ".chars().cycle().into_iter())
            .take(length as usize)
            .enumerate()
        {
            if i == fore_length as usize {
                queue!(stdout, SetBackgroundColor(back_color)).expect("set back color");
            }

            queue!(stdout, Print(chr)).expect("print bar");
        }
    }

    fn firebolt(&self, coord: Coord) {
        let mut stdout = stdout();

        queue!(
            stdout,
            MoveTo(
                coord.col as u16 + Self::PADDING_LEFT,
                coord.row as u16 + Self::PADDING_TOP
            ),
            SetForegroundColor(Color::Red),
            SetBackgroundColor(Color::DarkRed),
            Print('*'),
        )
        .expect("print firebolt");

        stdout.flush().expect("flush output to stdout");
    }

    fn lightning(&self, coord: Coord) {
        let mut stdout = stdout();

        queue!(
            stdout,
            MoveTo(
                coord.col as u16 + Self::PADDING_LEFT,
                coord.row as u16 + Self::PADDING_TOP
            ),
            SetForegroundColor(Color::DarkCyan),
            SetBackgroundColor(Color::Cyan),
            Print('~'),
        )
        .expect("print lightning");

        stdout.flush().expect("flush output to stdout");
    }
}
