use crate::dictionary::*;
use game::{Actor, ActorEvent, Event, Item, ItemEvent, Stat, WorldView};

pub fn log_event(world_view: &WorldView, effect: &Event) -> Option<String> {
    let player_key = world_view.pov_key();
    match *effect {
        Event::Actor(actor_key, ref actor_event) if actor_key == player_key => {
            if let Some(player) = world_view.actor(actor_key).expect("actor") {
                log_player_event(player, actor_event)
            } else {
                None
            }
        }
        Event::Actor(actor_key, ref actor_event) => {
            if let Some(actor) = world_view.actor(actor_key).expect("actor") {
                log_actor_event(actor, actor_event)
            } else {
                None
            }
        }
        Event::Item(item_key, ref item_event) => {
            let item = world_view.item(item_key).expect("item").expect("see item");
            log_item_event(item, item_event)
        }
        Event::MeleeAttack {
            actor_key,
            target_key,
        } if actor_key == player_key => {
            if let Some(target_name) = world_view.actor(target_key).expect("actor").map(actor_name)
            {
                Some(format!("You attack the {}", target_name))
            } else {
                unreachable!();
                // format!("You attack something", attacker_name)
            }
        }
        Event::MeleeAttack {
            actor_key,
            target_key,
        } if target_key == player_key && world_view.see_actor(actor_key).expect("see attacker") => {
            let attacker_name = world_view
                .actor(actor_key)
                .expect("actor")
                .map(actor_name)
                .expect("name");
            Some(format!("The {} attacks you", attacker_name))
        }
        Event::MeleeHit {
            actor_key,
            target_key,
        } if actor_key == player_key && world_view.see_actor(target_key).expect("see target") => {
            Some(String::from("and hit it."))
        }
        Event::MeleeHit {
            actor_key,
            target_key,
        } if target_key == player_key && world_view.see_actor(actor_key).expect("see actor") => {
            Some(String::from("and hits you."))
        }
        Event::MeleeMiss {
            actor_key,
            target_key,
        } if actor_key == player_key && world_view.see_actor(target_key).expect("see actor") => {
            Some(String::from("but miss it."))
        }
        Event::MeleeMiss {
            actor_key,
            target_key,
        } if target_key == player_key && world_view.see_actor(actor_key).expect("see actor") => {
            Some(String::from("but misses."))
        }
        Event::Throw {
            actor_key,
            item_key,
            ..
        } if actor_key == player_key => {
            if let Some(item) = world_view.item(item_key).expect("item") {
                Some(format!("You throw a {}.", item_name(item)))
            } else {
                unreachable!()
            }
        }
        Event::Pick {
            actor_key,
            item_key,
        } if actor_key == player_key => {
            let item_name = world_view
                .pov_actor()
                .item(item_key)
                .map(item_name)
                .expect("name");
            Some(format!("You pick up a {}", item_name))
        }
        Event::DropItem {
            actor_key,
            item_key,
        } if actor_key == player_key => {
            if let Some(item_name) = world_view.item(item_key).expect("item").map(item_name) {
                Some(format!("You drop down a {}", item_name))
            } else {
                None
            }
        }
        // Event::Read {
        //     actor_key,
        //     scroll_key,
        // } if actor_key == player_key => {
        //     if let Some(item_name) = world_view.item(scroll_key).expect("item").map(item_name) {
        //         Some(format!("You read a {}", item_name))
        //     } else {
        //         None
        //     }
        // }
        _ => None,
    }
}

pub fn log_player_event(player: &Actor, actor_event: &ActorEvent) -> Option<String> {
    match *actor_event {
        ActorEvent::Blessed(_) => Some(String::from("Your prayers are answered:")),
        ActorEvent::CastFireBolt => Some(String::from("You cast a fire bolt!")),
        ActorEvent::CastLightning => Some(String::from("You cast a lightning bolt!")),
        ActorEvent::DescendStairs => Some(String::from("You venture deeper into the Dungeon.")),
        ActorEvent::Die => Some(String::from("The Bottomless Dungeon claims another victim!")),
        ActorEvent::Heal => Some(String::from("You feel so much better!")),
        ActorEvent::Jump => Some(String::from("You jump into the darkness.")),
        ActorEvent::Fall => Some(String::from("You fall hard on the lower level's floor.")),
        ActorEvent::Fulminate => Some(String::from("You get hit by a lightning")),
        ActorEvent::Quaff(potion) => {
            let potion_name = potion_name(potion);
            Some(format!("You drink a {}.", potion_name))
        }
        ActorEvent::TakeDamage(damage) => Some(format!(
            "[{} hp, {}]",
            damage.amount,
            damage_name(damage.kind)
        )),
        ActorEvent::Upgrade(stat) => match stat {
            Stat::Con => Some(String::from("you feel healthier!")),
            Stat::Int => Some(String::from("your grow smarter!")),
            Stat::Dex => Some(String::from("you feel more dexterous!")),
            Stat::Per => Some(String::from("your senses are heightened!")),
            Stat::Str => Some(String::from("you grow stronger!")),
        },
        ActorEvent::Wear(item_key) => {
            let item_name = player.item(item_key).map(item_name).expect("name");
            Some(format!("You wear a {}", item_name))
        }
        ActorEvent::Wield(item_key) => {
            let item_name = player.item(item_key).map(item_name).expect("name");
            Some(format!("You wield a {}", item_name))
        }
        _ => None,
    }
}

pub fn log_actor_event(actor: &Actor, actor_event: &ActorEvent) -> Option<String> {
    let actor_name = actor_name(actor);

    match *actor_event {
        ActorEvent::Die => Some(format!("The {} dies.", actor_name)),
        ActorEvent::Fulminate => Some(format!("The {} is hit by a lightning.", actor_name)),
        ActorEvent::TakeDamage(damage) => Some(format!(
            "[{} hp, {}]",
            damage.amount,
            damage_name(damage.kind)
        )),
        _ => None,
    }
}

pub fn log_item_event(item: &Item, item_event: &ItemEvent) -> Option<String> {
    let item_name = item_name(item);

    match item_event {
        ItemEvent::Disappear => Some(format!("The {} disappears into the chasm.", item_name)),
        ItemEvent::PotionShatter => Some(format!(
            "The {} shatters, splashing its content on the floor.",
            item_name
        )),
        _ => None,
    }
}

pub fn log_new_line(event: &Event) -> bool {
    match event {
        Event::Actor(_, actor_event) => match actor_event {
            ActorEvent::TakeDamage(..) | ActorEvent::Upgrade(..) => false,
            _ => true,
        },
        Event::MeleeHit { .. } | Event::MeleeMiss { .. } => false,
        _ => true,
    }
}
