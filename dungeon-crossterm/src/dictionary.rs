use crossterm::style::Color;
use game::{
    Actor, ActorKind, Armor, DamageKind, Dir, Element, Feature, FloorKind, Item, ItemKind,
    PhysicalDamage, Potion, Scroll, Stat, Tile, TileKind, WallKind, Wand, Weapon,
};

pub const DUNGEON_INTRO: &str = "You have entered the Bottomless Dungeon, you fool!";

pub const JUMP_CONFIRM: &str =
    "Are you sure you want to jump down? You will get injured in the fall and might even die!";
pub const YES: &str = "YES";
pub const NO: &str = "NO";

pub fn dir_name(dir: Dir) -> &'static str {
    match dir {
        Dir::North => "North",
        Dir::West => "West",
        Dir::South => "South",
        Dir::East => "East",
    }
}

pub fn stat_name(stat: Stat) -> &'static str {
    match stat {
        Stat::Con => "Constitution",
        Stat::Dex => "Dexterity",
        Stat::Int => "Intelligence",
        Stat::Per => "Perception",
        Stat::Str => "Strength",
    }
}

pub fn tile_background_color(tile: &Tile) -> Color {
    if let Some(feature) = tile.feature() {
        match feature {
            Feature::Altar { used: false } => Color::Yellow,
            Feature::Blood => Color::DarkRed,
            Feature::Door { open: false } => Color::DarkYellow,
            Feature::Fire { .. } => Color::Red,
            Feature::Flooded => Color::Blue,
            Feature::Vegetation { .. } => Color::Green,
            _ => tilekind_background_color(tile.kind()),
        }
    } else {
        tilekind_background_color(tile.kind())
    }
}

pub fn tilekind_background_color(tile_kind: TileKind) -> Color {
    match tile_kind {
        TileKind::Floor(floor_kind) => match floor_kind {
            FloorKind::Plank => Color::Yellow,
            FloorKind::Soil => Color::White,
            FloorKind::Stone => Color::Grey,
        },
        TileKind::Wall(wall_kind) => match wall_kind {
            WallKind::Stone => Color::DarkGrey,
            WallKind::Brick => Color::DarkRed,
            WallKind::Molded => Color::DarkGreen,
        },
        TileKind::Stairs => Color::Grey,
        TileKind::Chasm => Color::Grey,
    }
}

pub fn tile_symb(tile: Tile) -> char {
    if let Some(feature) = tile.feature() {
        match feature {
            Feature::Altar { .. } => '±',
            Feature::Door { open: true } => '_',
            Feature::Door { open: false } => '+',
            Feature::Fire { .. } => '^',
            Feature::Vegetation { trampled: true } => '"',
            Feature::Vegetation { trampled: false } => '&',
            _ => tilekind_symb(tile.kind()),
        }
    } else {
        tilekind_symb(tile.kind())
    }
}

pub fn tilekind_symb(tile_kind: TileKind) -> char {
    match tile_kind {
        TileKind::Floor(_) => '·',
        TileKind::Wall(_) => '#',
        TileKind::Chasm => '░',
        TileKind::Stairs => '>',
    }
}

pub fn feature_name(feature: Feature) -> &'static str {
    match feature {
        Feature::Altar { .. } => "altar",
        Feature::Blood => "blood",
        Feature::Door { open: true } => "open door",
        Feature::Door { open: false } => "closed door",
        Feature::Fire { .. } => "fire",
        Feature::Flooded => "flooded",
        Feature::Vegetation { trampled: true } => "trampled vegetation",
        Feature::Vegetation { trampled: false } => "dense vegetation",
    }
}

pub fn tilekind_name(tile_kind: TileKind) -> &'static str {
    match tile_kind {
        TileKind::Floor(_) => "floor",
        TileKind::Wall(_) => "wall",
        TileKind::Chasm => "chasm",
        TileKind::Stairs => "stairs",
    }
}

pub fn actor_symb(actor: &Actor) -> char {
    match actor.kind() {
        ActorKind::Player => '@',
        ActorKind::Bat => 'B',
        ActorKind::Rat => 'R',
        ActorKind::Ork => 'O',
        ActorKind::Goblin => 'G',
        ActorKind::Troll => 'T',
        ActorKind::Zombie => 'Z',
    }
}

pub fn actor_name(actor: &Actor) -> &'static str {
    match actor.kind() {
        ActorKind::Player => "rogue",
        ActorKind::Bat => "bat",
        ActorKind::Rat => "rat",
        ActorKind::Ork => "ork",
        ActorKind::Goblin => "goblin",
        ActorKind::Troll => "troll",
        ActorKind::Zombie => "zombie",
    }
}

pub fn item_symb(item: &Item) -> char {
    match item.kind() {
        ItemKind::Armor(_) => '[',
        ItemKind::Potion(_) => '!',
        ItemKind::Scroll(_) => '?',
        ItemKind::Wand(_) => '\\',
        ItemKind::Weapon(_) => '/',
    }
}

pub fn item_name(item: &Item) -> &'static str {
    match item.kind() {
        ItemKind::Potion(potion) => potion_name(potion),
        ItemKind::Scroll(scroll) => match scroll {
            Scroll::AScroll => "scroll",
        },
        ItemKind::Wand(wand) => match wand {
            Wand::FireBolt => "firebolt wand",
            Wand::Lightning => "lightning wand",
        },
        ItemKind::Armor(armor) => match armor {
            Armor::Leather => "leather armor",
            Armor::Chainmail => "chainmail armor",
            Armor::Plate => "plate armor",
        },
        ItemKind::Weapon(weapon) => match weapon {
            Weapon::Dagger => "dagger",
            Weapon::Sword => "sword",
            Weapon::WarHammer => "war hammer",
        },
    }
}

pub fn potion_name(potion: Potion) -> &'static str {
    match potion {
        Potion::Health => "health potion",
    }
}

pub fn item_description(item: &Item) -> &'static str {
    match item.kind() {
        ItemKind::Potion(potion) => match potion {
            Potion::Health => "A flask swirling with crimson liquid.",
        },
        ItemKind::Scroll(scroll) => match scroll {
            Scroll::AScroll => "A parchment scroll covered in dwarven runes.",
        },
        ItemKind::Wand(wand) => match wand {
            Wand::FireBolt => "A wooden wand.",
            Wand::Lightning => "A metallic wand.",
        },
        ItemKind::Armor(armor) => match armor {
            Armor::Leather => "A hardened leather jacket.",
            Armor::Chainmail => "A chainmail armor.",
            Armor::Plate => "A heavy plate armor.",
        },
        ItemKind::Weapon(weapon) => match weapon {
            Weapon::Dagger => "A thin and pointy dagger, excellent for stabbing.",
            Weapon::Sword => "A broad sword, with a razor-sharp edge.",
            Weapon::WarHammer => "A war hammer, heavy enough to crush a ork's head.",
        },
    }
}

pub fn damage_name(damage_kind: DamageKind) -> &'static str {
    match damage_kind {
        DamageKind::Physical(physical_damage) => match physical_damage {
            PhysicalDamage::Bludgeoning => "bludgeoning",
            PhysicalDamage::Piercing => "piercing",
            PhysicalDamage::Slashing => "slashing",
        },
        DamageKind::Elemental(elemental_damage) => match elemental_damage {
            Element::Electricity => "electricity",
            Element::Fire => "fire",
            Element::Cold => "cold",
        },
    }
}
